package com.project.centroid.newfeedapp.fragments;


import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.centroid.newfeedapp.NewsModel;
import com.project.centroid.newfeedapp.R;
import com.project.centroid.newfeedapp.adapter.NewsRecyclerAdapter;
import com.project.centroid.newfeedapp.model.NewsItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSports extends Fragment {
    RecyclerView recyclerView;
    private List<NewsItem> list;

    public FragmentSports() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_blank, container, false);
        recyclerView =(RecyclerView)v.findViewById(R.id.reyclerview);
        inflateRecycler(v);
        return v;
    }

    private void inflateRecycler(View v)
    {
        NewsRecyclerAdapter recyclerAdapter=new NewsRecyclerAdapter(getActivity(),list);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        //recyclerView.setLayoutAnimation(new De);
        recyclerView.setAdapter(recyclerAdapter);

    }
}
