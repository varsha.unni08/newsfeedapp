package com.project.centroid.newfeedapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;
import com.google.gson.Gson;
import com.project.centroid.newfeedapp.NewsDetailsActivity;
import com.project.centroid.newfeedapp.R;
import com.project.centroid.newfeedapp.model.NewsItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<NewsItem> newList;
    Context mContext;
    NewsItem featuredUser;
    private boolean isSwitchView = true;
    private static final int LIST_ITEM = 0;
    private static final int GRID_ITEM = 1;

    private final int VIEW_PROG = 2;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    public NewsListAdapter(Context context, List<NewsItem> users, RecyclerView recyclerView) {
        this.newList = users;
        this.mContext = context;

        Log.v("TAG r",""+recyclerView.getLayoutManager());
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {


            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    totalItemCount = linearLayoutManager.getItemCount();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }else  if (recyclerView.getLayoutManager() instanceof GridLayoutManager){

            final GridLayoutManager linearLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = null;
        RecyclerView.ViewHolder vh=null;
        switch (i) {
            case LIST_ITEM:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
                vh = new CardViewHolder(v);
                break;
               case VIEW_PROG:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_progress_loadmore, viewGroup, false);
                vh = new ProgressViewHolder(v);
                break;
        }

        return vh;

    }

    @Override
    public int getItemViewType(int position) {

            return newList.get(position) != null ? LIST_ITEM:VIEW_PROG;
    }

    public boolean toggleItemViewType() {
        isSwitchView = !isSwitchView;
        return isSwitchView;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolderNew, final int i) {


        if (viewHolderNew instanceof CardViewHolder) {
            CardViewHolder holder = (CardViewHolder) viewHolderNew;

            try {

                final NewsItem currentItem = newList.get(i);


                // Get the news title information from the current news item and
                // set text on the news title {@link TextView}
                holder.newsTitleTextView.setText(currentItem.getDsNewsHead());

                // Get the news section information from the current news item
                // and set text on the section {@link TextView}
                holder.sectionNameTextView.setText(currentItem.getDsNewsContent());

                // Get the published date of the current news item information from the current news item
                // and set the same as text on the date published {@link TextView}
                holder.datePublishedTextView.setText(""+currentItem.getTotalReviews());


                // Register and setup listener to open up news story in web browser
                holder.storyCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent=new Intent(v.getContext(), NewsDetailsActivity.class);
                        intent.putExtra("DATA",new Gson().toJson(currentItem));
                        v.getContext().startActivity(intent);
                    }
                });

                if(currentItem.getNewsRating()!=null&&!TextUtils.isEmpty(currentItem.getNewsRating())){
                    holder.ratingBar.setRating(Float.parseFloat(currentItem.getNewsRating()));

                }
                // Check whether or not the current news item has a thumbnail or not
                if (currentItem.getDsImage() != null) {
                    // The current news item does not have thumbnail information
                    // Set scale type for the default image
                    try{
                        Glide.with(mContext).load(currentItem.getDsImage()).into(holder.newsThumbnail);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    // Set the default image on the {@link ImageView} for the thumbnail
                    //holder.newsThumbnail.setImageResource(R.drawable.no_thumbnail);
                } else {
                    // The current news item has thumbnail information
                    holder.newsThumbnail.setScaleType(ImageView.ScaleType.CENTER_CROP);

                    // Get the bitmap thumbnail from the current news item and
                    // Set it as the image on the {@link ImageView} thumbnail
                    // holder.newsThumbnail.setImageBitmap(currentItem.getThumbnail());
                }


                //viewHolder.mShimmerViewContainer.startShimmer();
                //viewHolder.mShimmerViewContainer.stopShimmer();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (viewHolderNew instanceof ProgressViewHolder) {
            ProgressViewHolder viewHolder = (ProgressViewHolder) viewHolderNew;

             viewHolder.progressBar.setIndeterminate(true);
        }

    }


    @Override
    public int getItemCount() {

        return (null != newList ? newList.size() : 0);
        //   return 5;
    }

    public  static class CardViewHolder extends RecyclerView.ViewHolder {

        /** {@link ImageView} for the news story's thumbnail */
        ImageView newsThumbnail;

        /** {@link TextView} for the title of the news story */
        TextView newsTitleTextView;

        /** {@link TextView} for the section of the news story */
        TextView sectionNameTextView;

        /** {@link TextView} for the published date of the news story */
        TextView datePublishedTextView;

        /** {@link CardView} for every news story */
        LinearLayout storyCard;
        RatingBar ratingBar;

        /** ItemView to cache reference hooks to the view elements of the recycler view */
        CardViewHolder(View itemView) {
            super(itemView);

            // Find the {@ink ImageView} for the thumbnail
            newsThumbnail = itemView.findViewById(R.id.story_image);
            ratingBar = itemView.findViewById(R.id.rating);

            // Find the {@link TextView} for the news title
            newsTitleTextView = itemView.findViewById(R.id.news_title_text);

            // Find the {@link TextView} for the section of the news story
            sectionNameTextView = itemView.findViewById(R.id.section_name_text);

            // Find the {@link TextView} for the published date
            datePublishedTextView = itemView
                    .findViewById(R.id.date_published_text);

            // Find the {@link CardView} for each news story
            storyCard = itemView.findViewById(R.id.story_card);
        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(@NonNull View v) {
            super(v);
            progressBar = v.findViewById(R.id.progress);
        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    public interface OnLoadMoreListener {
        void onLoadMore();
    }
    public void setLoaded() {
        loading = false;
    }

    public void setLoadedFalse() {
        loading = true;
    }
}
