package com.project.centroid.newfeedapp;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.project.centroid.newfeedapp.adapter.NewsPagerAdapter;
import com.project.centroid.newfeedapp.application.MyApplication;
import com.project.centroid.newfeedapp.fragments.FragmentSport;
import com.project.centroid.newfeedapp.model.Category;
import com.project.centroid.newfeedapp.model.District;
import com.project.centroid.newfeedapp.model.Language;
import com.project.centroid.newfeedapp.model.Pincode;
import com.project.centroid.newfeedapp.model.State;
import com.project.centroid.newfeedapp.model.VillageModel;
import com.project.centroid.newfeedapp.reciever.NetworkChangeReceiver;
import com.project.centroid.newfeedapp.rest.ApiConstants;
import com.project.centroid.newfeedapp.rest.RestService;
import com.project.centroid.newfeedapp.rest.Utils;
import com.project.centroid.newfeedapp.utils.LocaleHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, NetworkChangeReceiver.ConnectivityReceiverListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private int mYear, mMonth, mDay, mHour, mMinute;
    Button btncall, btnnearby, btncustomercare;
    public NewsPagerAdapter mSectionsPagerAdapter;
    AppCompatSpinner rSpinner, spinnerState, spinnerDist, spinnerPin, spinnerVillage;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    public ViewPager mViewPager;
    public TabLayout tabLayout;
    private SharedPreferences sharedpreferences;
    private DrawerLayout drawer;
    private Snackbar mSnackBar;
    private Button btnsub;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedpreferences = this.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
        try {
            if (!TextUtils.isEmpty(sharedpreferences.getString(ApiConstants.USERLANG, ""))) {
                String lan = sharedpreferences.getString(ApiConstants.USERLANG, "");
                LocaleHelper.setLocale(MainActivity.this, lan);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mSectionsPagerAdapter = new NewsPagerAdapter(this, getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout = findViewById(R.id.sliding_tabs);


        LayoutInflater inflator = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(this, R.color.colorAccent));
        tabLayout.setTabTextColors(
                ContextCompat.getColor(this, android.R.color.white),
                ContextCompat.getColor(this, android.R.color.black)
        );

        tabLayout.setupWithViewPager(mViewPager);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        mSnackBar = Snackbar.make(findViewById(android.R.id.content), "Please press again to exit!", Snackbar.LENGTH_SHORT);


    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().setConnectivityListener(this);

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        searchViewItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem) {

                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                String date = dayOfMonth + "-" + monthOfYear + "-" + year;
                                String query = null;
                                try {
                                    Log.v("TAG", "-->" + date);
                                    Calendar c = Calendar.getInstance();
                                    c.set(year, monthOfYear, dayOfMonth);
                                    query = dateFormat.format(c.getTime());
                                    Log.v("TAG", "-->" + query);
                                    Intent intent = new Intent(getApplicationContext(), SearchResultActivity.class);
                                    intent.putExtra("key", query);
                                    startActivity(intent);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                // .setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.setButton(DatePickerDialog.BUTTON_POSITIVE, getString(R.string.ok), datePickerDialog);
                datePickerDialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, getString(R.string.cancel), datePickerDialog);
                datePickerDialog.show();
                return false;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                return false;
            }
        });
     /*   final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchViewAndroidActionBar.clearFocus();
                Intent intent = new Intent(getApplicationContext(), SearchResultActivity.class);
                intent.putExtra("key", query);
                startActivity(intent);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });*/
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.settings) {
            startActivity(new Intent(getApplicationContext(), Login.class));
            return true;
        }
        if (id == R.id.filter) {
            AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
            builder.setTitle(getString(R.string.nav_opt8)).setMessage(getString(R.string.string_reporter)).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {


                }
            }).setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            }).create().show();
            //showFilterDialog();
            return true;
        }
        if (id == R.id.call) {
            showCallDialog();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showFilterDialog() {
        if (NetworkChangeReceiver.isConnected()) {
            sharedpreferences = this.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);

            categoryBYAll();
            getStates();
        }

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_filter, null);
      /*  final EditText etUsername = alertLayout.findViewById(R.id.et_username);
        final EditText etEmail = alertLayout.findViewById(R.id.et_email);
        final CheckBox cbToggle = alertLayout.findViewById(R.id.cb_show_pass);*/

        rSpinner = alertLayout.findViewById(R.id.rSpinner);
        spinnerPin = alertLayout.findViewById(R.id.spinnerPin);
        spinnerState = alertLayout.findViewById(R.id.spinnerState);
        spinnerDist = alertLayout.findViewById(R.id.spinnerDist);
        spinnerVillage = alertLayout.findViewById(R.id.spinnerVillage);
        btnsub = alertLayout.findViewById(R.id.btnsub);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(getString(R.string.nav_settings));
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        //alert.setCancelable(false);
        if (NetworkChangeReceiver.isConnected()) {
            sharedpreferences = this.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);

            categoryBYAll();
            getStates();


            btnsub.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (rSpinner != null && rSpinner.getCount() > 0) {
                        String id = ((Language) rSpinner.getSelectedItem()).getCdNewsCategory();
                        String lan = Utils.checkLanguage(id);

                        LocaleHelper.setLocale(MainActivity.this, lan);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(ApiConstants.USERLANG, lan);

                        editor.apply();
                        editor.commit();

                        //It is required to recreate the activity to reflect the change in UI.
                        recreate();
                    }
                }
            });
        }
        AlertDialog dialog = alert.create();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);


    }

    private void showCallDialog() {
        if (NetworkChangeReceiver.isConnected()) {
            sharedpreferences = this.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);

            categoryBYAll();
            getStates();
        }

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_call_nearby, null);
      /*  final EditText etUsername = alertLayout.findViewById(R.id.et_username);
        final EditText etEmail = alertLayout.findViewById(R.id.et_email);
        final CheckBox cbToggle = alertLayout.findViewById(R.id.cb_show_pass);*/


        btncall = alertLayout.findViewById(R.id.btncall);
        btnnearby = alertLayout.findViewById(R.id.btnnearby);
        btncustomercare = alertLayout.findViewById(R.id.btncustomercare);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(getString(R.string.emergency_call));
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        //alert.setCancelable(false);

        sharedpreferences = this.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        btncall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:918660481428"));

                    /*if (ActivityCompat.checkSelfPermission(MainActivity.this,
                            Manifest.permission.DA) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }*/
                    startActivity(callIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        AlertDialog dialog = alert.create();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.live_news:
                ApiConstants.TYPE = "live";
                startActivity(new Intent(this, LiveNewsActivity.class));
                break;
            case R.id.nav_latest:
                ApiConstants.TYPE = "latest";
                startActivity(new Intent(this, NewsView.class));
                break;
            case R.id.nav_local:
                ApiConstants.TYPE = "All";
                startActivity(new Intent(this, NewsView.class));
                break;
            case R.id.nav_profile:
                startActivity(new Intent(this, MyProfile.class));
                break;
            case R.id.nav_about:

                break;

                case R.id.nav_applyreporter:
                    AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle(getString(R.string.nav_opt0)).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {


                        }
                    }).setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    }).create().show();
                break;
            case R.id.nav_review:
                startActivity(new Intent(this, ViewCommentsActivity.class));

                break;
            case R.id.nav_visit:
                startActivity(new Intent(this, WebViewActivity.class));

                break;

            case R.id.nav_home:
                Utils.sId = null;
                Utils.dId = null;
                Utils.pId = null;
                startActivity(new Intent(this, Allnews.class));

                break;

            case R.id.nav_settings:
                startActivity(new Intent(this, Settings.class));

                break;
            case R.id.nav_logout:
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(ApiConstants.LOGINSTATE, "");
                editor.apply();
                editor.commit();
                startActivity(new Intent(getApplicationContext(), Login.class));
                finish();
                break;


        }
        drawer.closeDrawer(GravityCompat.START);
        return false;

    }

    private void category() {

        final ProgressDialog progressDialog = new ProgressDialog(this,
                ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<Category>> callback = client.getNewsCategory(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""));
        callback.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {

                Log.v("TAG", response.body().toString());
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    try {

                        for (int i = 0; i < response.body().size(); i++) {

                            FragmentSport fView = new FragmentSport();
                            View view = fView.getView();

                            // mSectionsPagerAdapter.addFrag(fView, response.body().get(i).getDsNewsCategory());
                        }

                        mViewPager.setAdapter(mSectionsPagerAdapter);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            try {
                if (mSnackBar.isShown()) {
                    Log.v("TAG", "v1");
                    super.onBackPressed();
                } else {
                    Log.v("TAG", "v2");
                    mSnackBar.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void categoryBYAll() {
        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<Language>> callback = client.getLanguages();
        callback.enqueue(new Callback<List<Language>>() {
            @Override
            public void onResponse(Call<List<Language>> call, Response<List<Language>> response) {

                Log.v("TAG", response.body().toString());
                if (response.isSuccessful()) {

                    try {

                        List<String> list = new ArrayList<>();
                        list.add(0, "Select");
                        for (int i = 0; i < response.body().size(); i++) {
                            list.add(response.body().get(i).getDsNewsCategory());

                        }
                        ArrayAdapter<Language> adapter =
                                new ArrayAdapter<Language>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, response.body());
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        rSpinner.setAdapter(adapter);
                        ((TextView) rSpinner.getSelectedItem()).setTextColor(getResources().getColor(R.color.colorPrimaryText));


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<List<Language>> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }

    private void addLanguage() {
        final ProgressDialog progressDialog = new ProgressDialog(this,
                ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<ResponseBody> callback = client.switchLanguage(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), ((Language) rSpinner.getSelectedItem()).getCdNewsCategory());
        callback.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    progressDialog.dismiss();
                    progressDialog.cancel();
                    JSONArray jsonArray = new JSONArray(response.body().string());
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        Snackbar.make(findViewById(android.R.id.content), "Language changed successfully!", Snackbar.LENGTH_SHORT).show();

                    } else {

                        Snackbar.make(findViewById(android.R.id.content), "Something went  wrong.Please try after some time!", Snackbar.LENGTH_SHORT).show();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                progressDialog.cancel();
            }
        });
    }

    private void addPincode() {
        final ProgressDialog progressDialog = new ProgressDialog(this,
                ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();
        if (spinnerPin != null && spinnerPin.getSelectedItem() != null) {
            String pin = ((Pincode) spinnerPin.getSelectedItem()).getDsPincode();
            Call<ResponseBody> callback = client.switchPinCode(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), pin);
            callback.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    try {
                        progressDialog.dismiss();
                        progressDialog.cancel();
                        JSONArray jsonArray = new JSONArray(response.body().string());
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                            Snackbar.make(findViewById(android.R.id.content), "Pincode changed successfully!", Snackbar.LENGTH_SHORT).show();

                        } else {

                            Snackbar.make(findViewById(android.R.id.content), "Something went  wrong.Please try after some time!", Snackbar.LENGTH_SHORT).show();

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progressDialog.dismiss();
                    progressDialog.cancel();
                }
            });
        }
    }

    private void getStates() {

        final ProgressDialog progressDialog = new ProgressDialog(this,
                ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("please wait...");
        progressDialog.show();


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<State>> callback = client.getAllStates(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), "state");
        callback.enqueue(new Callback<List<State>>() {
            @Override
            public void onResponse(Call<List<State>> call, Response<List<State>> response) {
                try {
                    progressDialog.cancel();
                    progressDialog.dismiss();
                    String responseBody = response.body().toString();
                    if (responseBody.trim().charAt(0) == '{') {

                    } else if (responseBody.trim().charAt(0) == '[') {


                        ArrayAdapter<State> adapter =
                                new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_spinner_dropdown_item, response.body());
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        spinnerState.setAdapter(adapter);
                      /*  if(Utils.sId!=null){
                            state.setSelection(Utils.sId);
                        }*/
                        // ((TextView) state.getSelectedItem).setTextColor(getResources().getColor(R.color.colorPrimaryText));
                        try {

                            spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    getDistrict();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<State>> call, Throwable t) {
                progressDialog.cancel();
                progressDialog.dismiss();
            }
        });
    }

    private void getDistrict() {

        final ProgressDialog progressDialog = new ProgressDialog(this,
                ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        String stat = ((State) spinnerState.getSelectedItem()).getCdState();
        if (stat != null) {
            // TODO: Implement your own authentication logic here.
            RestService.RestApiInterface client = RestService.getClient();

            Call<List<District>> callback = client.getAllDistrict(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), "district", stat);
            callback.enqueue(new Callback<List<District>>() {
                @Override
                public void onResponse(Call<List<District>> call, Response<List<District>> response) {

                    try {
                        try {
                            progressDialog.cancel();
                            progressDialog.dismiss();
                            String responseBody = response.body().toString();
                            if (responseBody.trim().charAt(0) == '{') {

                            } else if (responseBody.trim().charAt(0) == '[') {


                                ArrayAdapter<District> adapter =
                                        new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_spinner_dropdown_item, response.body());
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                spinnerDist.setAdapter(adapter);
                               /* if(Utils.dId!=null){
                                    dist.setSelection(Utils.dId);}*/
                                //((TextView) dist.getSelectedItem()).setTextColor(getResources().getColor(R.color.colorPrimaryText));
                                spinnerDist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        getPincode();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<List<District>> call, Throwable t) {
                    progressDialog.cancel();
                    progressDialog.dismiss();
                }
            });
        }
    }

    private void getPincode() {

        final ProgressDialog progressDialog = new ProgressDialog(this,
                ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        final String pin = ((District) spinnerDist.getSelectedItem()).getCdDistrict();
        if (pin != null) {
            // TODO: Implement your own authentication logic here.
            RestService.RestApiInterface client = RestService.getClient();

            Call<List<Pincode>> callback = client.getAllPincode(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), "pincode", pin);
            callback.enqueue(new Callback<List<Pincode>>() {
                @Override
                public void onResponse(Call<List<Pincode>> call, Response<List<Pincode>> response) {

                    try {
                        progressDialog.cancel();
                        progressDialog.dismiss();
                        if (response != null) {
                            String responseBody = response.body().toString();
                            if (responseBody.trim().charAt(0) == '{') {

                            } else if (responseBody.trim().charAt(0) == '[') {


                                ArrayAdapter<Pincode> adapter =
                                        new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_spinner_dropdown_item, response.body());
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                spinnerPin.setAdapter(adapter);
                                spinnerPin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        getVillage();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });
                                /*if(Utils.pId!=null){
                                    pincode.setSelection(Utils.pId);
                                }*/
                                //((TextView) state.getSelectedItem()).setTextColor(getResources().getColor(R.color.colorPrimaryText));

                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<List<Pincode>> call, Throwable t) {
                    progressDialog.cancel();
                    progressDialog.dismiss();
                }
            });
        }
    }

    private void getVillage() {

        final ProgressDialog progressDialog = new ProgressDialog(this,
                ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        String pin = ((Pincode) spinnerPin.getSelectedItem()).getCdPincode();
        if (pin != null) {
            // TODO: Implement your own authentication logic here.
            RestService.RestApiInterface client = RestService.getClient();

            Call<List<VillageModel>> callback = client.getAllVillage(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), "village", pin);
            callback.enqueue(new Callback<List<VillageModel>>() {
                @Override
                public void onResponse(Call<List<VillageModel>> call, Response<List<VillageModel>> response) {

                    try {
                        progressDialog.cancel();
                        progressDialog.dismiss();
                        if (response != null) {
                            String responseBody = response.body().toString();
                            if (responseBody.trim().charAt(0) == '{') {

                            } else if (responseBody.trim().charAt(0) == '[') {


                                ArrayAdapter<VillageModel> adapter =
                                        new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_spinner_dropdown_item, response.body());
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                spinnerVillage.setAdapter(adapter);
                                /*if(Utils.pId!=null){
                                    pincode.setSelection(Utils.pId);
                                }*/
                                //((TextView) state.getSelectedItem()).setTextColor(getResources().getColor(R.color.colorPrimaryText));

                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<List<VillageModel>> call, Throwable t) {
                    progressDialog.cancel();
                    progressDialog.dismiss();
                }
            });
        }
    }
}

