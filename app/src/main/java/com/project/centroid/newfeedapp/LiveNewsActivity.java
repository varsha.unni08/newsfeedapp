package com.project.centroid.newfeedapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.project.centroid.newfeedapp.adapter.RecyclerAdapterLiveNews;
import com.project.centroid.newfeedapp.adapter.ReviewRecyclerAdapter;
import com.project.centroid.newfeedapp.model.LiveNewsItem;
import com.project.centroid.newfeedapp.rest.ApiConstants;
import com.project.centroid.newfeedapp.rest.RestService;
import com.project.centroid.newfeedapp.utils.LocaleHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LiveNewsActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private SharedPreferences sharedpreferences;
    private ArrayList<LiveNewsItem> newsModelList;
    private RecyclerAdapterLiveNews recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedpreferences = this.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);

            try{
                if(!TextUtils.isEmpty(sharedpreferences.getString(ApiConstants.USERLANG, ""))){
                    String lan=sharedpreferences.getString(ApiConstants.USERLANG, "");
                    LocaleHelper.setLocale(LiveNewsActivity.this, lan);
            }
        }catch (Exception  e){
            e.printStackTrace();
        }

        setContentView(R.layout.activity_live_news);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getReviews();

    }

    public void startVideoActivity(String url) {
        Intent intent=new Intent(getApplicationContext(),VideoViewActivity.class);
        intent.putExtra("comefrom","2");
        intent.putExtra("url",url);
        startActivityForResult(intent,13);
    }

    private void getReviews() {
        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<LiveNewsItem>> callback = client.getLiveNews(ApiConstants.BEARER+sharedpreferences.getString(ApiConstants.USERTOKEN,""));
        callback.enqueue(new Callback<List<LiveNewsItem>>() {
            @Override
            public void onResponse(Call<List<LiveNewsItem>> call, Response<List<LiveNewsItem>> response) {


                if (response.isSuccessful()&&response!=null) {

                    try {

                        Log.v("TAG", response.body().toString());
                        newsModelList=new ArrayList<>();
                        newsModelList.addAll(response.body());
                        recyclerAdapter=new RecyclerAdapterLiveNews(newsModelList,LiveNewsActivity.this);
                        //recyclerView.setLayoutAnimation(new De);
                        recyclerView.setAdapter(recyclerAdapter);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }


            }


            @Override
            public void onFailure(Call<List<LiveNewsItem>> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }

}
