package com.project.centroid.newfeedapp.model;

import com.google.gson.annotations.SerializedName;


public class VillageModel {

    @SerializedName("ds_village")
    private String dsVillage;

    @SerializedName("cd_village")
    private String cdVillage;

    public void setDsVillage(String dsVillage) {
        this.dsVillage = dsVillage;
    }

    public String getDsVillage() {
        return dsVillage;
    }

    public void setCdVillage(String cdVillage) {
        this.cdVillage = cdVillage;
    }

    public String getCdVillage() {
        return cdVillage;
    }

    @Override
    public String toString() {
        return ""+this.dsVillage;
    }
}