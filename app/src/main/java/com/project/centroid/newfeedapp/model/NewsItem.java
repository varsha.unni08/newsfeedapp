package com.project.centroid.newfeedapp.model; ;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewsItem {

    @SerializedName("cd_news")
    @Expose
    private String cdNews;
    @SerializedName("ds_news_head")
    @Expose
    private String dsNewsHead;
    @SerializedName("ds_news_content")
    @Expose
    private String dsNewsContent;
    @SerializedName("ds_image")
    @Expose
    private String dsImage;
    @SerializedName("ds_video")
    @Expose
    private String dsVideo;
    @SerializedName("newsRating")
    @Expose
    private String newsRating;
    @SerializedName("totalReviews")
    @Expose
    private String totalReviews;

    /**
     * No args constructor for use in serialization
     *
     */
    public NewsItem() {
    }

    /**
     *
     * @param dsImage
     * @param dsVideo
     * @param dsNewsContent
     * @param cdNews
     * @param dsNewsHead
     * @param totalReviews
     * @param newsRating
     */
    public NewsItem(String cdNews, String dsNewsHead, String dsNewsContent, String dsImage, String dsVideo, String newsRating, String totalReviews) {
        super();
        this.cdNews = cdNews;
        this.dsNewsHead = dsNewsHead;
        this.dsNewsContent = dsNewsContent;
        this.dsImage = dsImage;
        this.dsVideo = dsVideo;
        this.newsRating = newsRating;
        this.totalReviews = totalReviews;
    }

    public String getCdNews() {
        return cdNews;
    }

    public void setCdNews(String cdNews) {
        this.cdNews = cdNews;
    }

    public String getDsNewsHead() {
        return dsNewsHead;
    }

    public void setDsNewsHead(String dsNewsHead) {
        this.dsNewsHead = dsNewsHead;
    }

    public String getDsNewsContent() {
        return dsNewsContent;
    }

    public void setDsNewsContent(String dsNewsContent) {
        this.dsNewsContent = dsNewsContent;
    }

    public String getDsImage() {
        return dsImage;
    }

    public void setDsImage(String dsImage) {
        this.dsImage = dsImage;
    }

    public String getDsVideo() {
        return dsVideo;
    }

    public void setDsVideo(String dsVideo) {
        this.dsVideo = dsVideo;
    }

    public String getNewsRating() {
        return newsRating;
    }

    public void setNewsRating(String newsRating) {
        this.newsRating = newsRating;
    }

    public String getTotalReviews() {
        return totalReviews;
    }

    public void setTotalReviews(String totalReviews) {
        this.totalReviews = totalReviews;
    }

}