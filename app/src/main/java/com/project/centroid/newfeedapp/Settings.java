package com.project.centroid.newfeedapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.project.centroid.newfeedapp.application.MyApplication;
import com.project.centroid.newfeedapp.model.District;
import com.project.centroid.newfeedapp.model.Language;
import com.project.centroid.newfeedapp.model.Pincode;
import com.project.centroid.newfeedapp.model.State;
import com.project.centroid.newfeedapp.model.VillageModel;
import com.project.centroid.newfeedapp.reciever.NetworkChangeReceiver;
import com.project.centroid.newfeedapp.rest.ApiConstants;
import com.project.centroid.newfeedapp.rest.RestService;
import com.project.centroid.newfeedapp.rest.Utils;
import com.project.centroid.newfeedapp.utils.LocaleHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Settings extends AppCompatActivity implements NetworkChangeReceiver.ConnectivityReceiverListener {

    @BindView(R.id.rTextlanguage)
    AppCompatTextView rTextlanguage;
    @BindView(R.id.rSpinner)
    AppCompatSpinner rSpinner;
    @BindView(R.id.lEditPincode)
    EditText lEditPincode;
    @BindView(R.id.lTextPin)
    TextInputLayout lTextPin;
    @BindView(R.id.section_label)
    TextView sectionLabel;
    @BindView(R.id.spinnerState)
    Spinner state;
    @BindView(R.id.section_dist)
    TextView sectionDist;
    @BindView(R.id.spinnerDist)
    Spinner dist;
    @BindView(R.id.section_pin)
    TextView sectionPin;
    @BindView(R.id.spinnerPin)
    Spinner pincode;
    @BindView(R.id.spinnerVillage)
    Spinner spinnerVillage;

    private SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


       sharedpreferences = this.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
        try{
            if(!TextUtils.isEmpty(sharedpreferences.getString(ApiConstants.USERLANG, ""))){
                String lan=sharedpreferences.getString(ApiConstants.USERLANG, "");
                LocaleHelper.setLocale(Settings.this, lan);
               // recreate();
            }}catch (Exception e){
            e.printStackTrace();
        }
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        if (NetworkChangeReceiver.isConnected()) {
            sharedpreferences = this.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);

            categoryBYAll();
            getStates();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().setConnectivityListener(this);

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }





    @OnClick({R.id.btnsub, R.id.btnSubmit})
    public void onViewClicked(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.btnSubmit:
                addLanguage();
                if(rSpinner!=null&&rSpinner.getCount()>0){
                    String id1=((Language)rSpinner.getSelectedItem()).getCdNewsCategory();
                    String lan=  Utils.checkLanguage(id1);

                    LocaleHelper.setLocale(Settings.this, lan);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(ApiConstants.USERLANG, lan);

                    editor.apply();
                    editor.commit();

                    //It is required to recreate the activity to reflect the change in UI.
                    recreate();
                }
                break;
            case R.id.btnsub:

                addPincode();
                break;
        }

    }
    private void categoryBYAll() {
        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<Language>> callback = client.getLanguages();
        callback.enqueue(new Callback<List<Language>>() {
            @Override
            public void onResponse(Call<List<Language>> call, Response<List<Language>> response) {

                Log.v("TAG", response.body().toString());
                if (response.isSuccessful()) {

                    try {

                        List<String> list = new ArrayList<>();
                        list.add(0, "Select");
                        for (int i = 0; i < response.body().size(); i++) {
                            list.add(response.body().get(i).getDsNewsCategory());

                        }
                        ArrayAdapter<Language> adapter =
                                new ArrayAdapter<Language>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, response.body());
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        rSpinner.setAdapter(adapter);
                       // ((TextView) rSpinner.getSelectedView()).setTextColor(getResources().getColor(R.color.colorPrimaryText));
                        try {
                            ((TextView) rSpinner.getSelectedView()).setTextColor(getResources().getColor(R.color.colorPrimaryText));
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<List<Language>> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }
    private void addLanguage() {


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<ResponseBody> callback = client.switchLanguage(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), ((Language) rSpinner.getSelectedItem()).getCdNewsCategory());
        callback.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {

                    JSONArray jsonArray = new JSONArray(response.body().string());
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        Snackbar.make(findViewById(android.R.id.content), "Language changed successfully!", Snackbar.LENGTH_SHORT).show();

                    } else {

                        Snackbar.make(findViewById(android.R.id.content), "Something went  wrong.Please try after some time!", Snackbar.LENGTH_SHORT).show();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void addPincode() {
       /* final ProgressDialog progressDialog = new ProgressDialog(this,
                ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();*/
        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();
        if (pincode != null && pincode.getSelectedItem() != null) {
            String pin = ((Pincode) pincode.getSelectedItem()).getDsPincode();
            Call<ResponseBody> callback = client.switchPinCode(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), pin);
            callback.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    try {
                  /*      progressDialog.dismiss();
                        progressDialog.cancel();*/
                        JSONArray jsonArray = new JSONArray(response.body().string());
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                            Snackbar.make(findViewById(android.R.id.content), "Pincode changed successfully!", Snackbar.LENGTH_SHORT).show();

                        } else {

                            Snackbar.make(findViewById(android.R.id.content), "Something went  wrong.Please try after some time!", Snackbar.LENGTH_SHORT).show();

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
/*                    progressDialog.dismiss();
                    progressDialog.cancel();*/
                }
            });
        }
    }

    private void getStates() {

      /*  final ProgressDialog progressDialog = new ProgressDialog(this,
                ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("please wait...");
        progressDialog.show();
*/

        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<State>> callback = client.getAllStates(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), "state");
        callback.enqueue(new Callback<List<State>>() {
            @Override
            public void onResponse(Call<List<State>> call, Response<List<State>> response) {
                try {
                   /* progressDialog.cancel();
                    progressDialog.dismiss();*/
                    String responseBody = response.body().toString();
                    if (responseBody.trim().charAt(0) == '{') {

                    } else if (responseBody.trim().charAt(0) == '[') {


                        ArrayAdapter<State> adapter =
                                new ArrayAdapter<>(Settings.this, android.R.layout.simple_spinner_dropdown_item, response.body());
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        state.setAdapter(adapter);
                      /*  if(Utils.sId!=null){
                            state.setSelection(Utils.sId);
                        }*/
                        // ((TextView) state.getSelectedItem).setTextColor(getResources().getColor(R.color.colorPrimaryText));
                        try {
                            ((TextView) state.getSelectedView()).setTextColor(getResources().getColor(R.color.colorPrimaryText));
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        try {

                            state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    getDistrict();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<State>> call, Throwable t) {
              /*  progressDialog.cancel();
                progressDialog.dismiss();*/
            }
        });
    }

    private void getDistrict() {

     /*   final ProgressDialog progressDialog = new ProgressDialog(this,
                ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
*/
        String stat = ((State) state.getSelectedItem()).getCdState();
        if (stat != null) {
            // TODO: Implement your own authentication logic here.
            RestService.RestApiInterface client = RestService.getClient();

            Call<List<District>> callback = client.getAllDistrict(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), "district", stat);
            callback.enqueue(new Callback<List<District>>() {
                @Override
                public void onResponse(Call<List<District>> call, Response<List<District>> response) {

                    try {
                        try {
            /*                progressDialog.cancel();
                            progressDialog.dismiss();*/
                            String responseBody = response.body().toString();
                            if (responseBody.trim().charAt(0) == '{') {

                            } else if (responseBody.trim().charAt(0) == '[') {


                                ArrayAdapter<District> adapter =
                                        new ArrayAdapter<>(Settings.this, android.R.layout.simple_spinner_dropdown_item, response.body());
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                dist.setAdapter(adapter);
                               /* if(Utils.dId!=null){
                                    dist.setSelection(Utils.dId);}*/
                                //((TextView) dist.getSelectedItem()).setTextColor(getResources().getColor(R.color.colorPrimaryText));
                                try {
                                    ((TextView) dist.getSelectedView()).setTextColor(getResources().getColor(R.color.colorPrimaryText));
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                dist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        getPincode();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<List<District>> call, Throwable t) {
//                    progressDialog.cancel();
//                    progressDialog.dismiss();
                }
            });
        }
    }

    private void getPincode() {

      /*  final ProgressDialog progressDialog = new ProgressDialog(this,
                ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();*/

        final String pin = ((District) dist.getSelectedItem()).getCdDistrict();
        if (pin != null) {
            // TODO: Implement your own authentication logic here.
            RestService.RestApiInterface client = RestService.getClient();

            Call<List<Pincode>> callback = client.getAllPincode(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), "pincode", pin);
            callback.enqueue(new Callback<List<Pincode>>() {
                @Override
                public void onResponse(Call<List<Pincode>> call, Response<List<Pincode>> response) {

                    try {
                      /*  progressDialog.cancel();
                        progressDialog.dismiss();*/
                        if (response != null) {
                            String responseBody = response.body().toString();
                            if (responseBody.trim().charAt(0) == '{') {

                            } else if (responseBody.trim().charAt(0) == '[') {


                                ArrayAdapter<Pincode> adapter =
                                        new ArrayAdapter<>(Settings.this, android.R.layout.simple_spinner_dropdown_item, response.body());
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                pincode.setAdapter(adapter);
                               pincode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        getVillage();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });
                                /*if(Utils.pId!=null){
                                    pincode.setSelection(Utils.pId);
                                }*/
                                //((TextView) state.getSelectedItem()).setTextColor(getResources().getColor(R.color.colorPrimaryText));
                                try {
                                    ((TextView) pincode.getSelectedView()).setTextColor(getResources().getColor(R.color.colorPrimaryText));
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<List<Pincode>> call, Throwable t) {
                   /* progressDialog.cancel();
                    progressDialog.dismiss();*/
                }
            });
        }
    }

    private void getVillage() {

   /*     final ProgressDialog progressDialog = new ProgressDialog(this,
                ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();*/

        String pin = ((Pincode) pincode.getSelectedItem()).getCdPincode();
        if (pin != null) {
            // TODO: Implement your own authentication logic here.
            RestService.RestApiInterface client = RestService.getClient();

            Call<List<VillageModel>> callback = client.getAllVillage(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), "village", pin);
            callback.enqueue(new Callback<List<VillageModel>>() {
                @Override
                public void onResponse(Call<List<VillageModel>> call, Response<List<VillageModel>> response) {

                    try {
                     /*   progressDialog.cancel();
                        progressDialog.dismiss();*/
                        if (response != null) {
                            String responseBody = response.body().toString();
                            if (responseBody.trim().charAt(0) == '{') {

                            } else if (responseBody.trim().charAt(0) == '[') {


                                ArrayAdapter<VillageModel> adapter =
                                        new ArrayAdapter<>(Settings.this, android.R.layout.simple_spinner_dropdown_item, response.body());
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                spinnerVillage.setAdapter(adapter);
                                /*if(Utils.pId!=null){
                                    pincode.setSelection(Utils.pId);
                                }*/
                                try {
                                    ((TextView) state.getSelectedView()).setTextColor(getResources().getColor(R.color.colorPrimaryText));
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<List<VillageModel>> call, Throwable t) {
                  /*  progressDialog.cancel();
                    progressDialog.dismiss();*/
                }
            });
        }
    }

}
