package com.project.centroid.newfeedapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

;

public class Language {

    @SerializedName("cd_news_language")
    @Expose
    private String cdNewsCategory;
    @SerializedName("ds_news_language")
    @Expose
    private String dsNewsCategory;

    /**
     * No args constructor for use in serialization
     *
     */
    public Language() {
    }

    /**
     *
     * @param cdNewsCategory
     * @param dsNewsCategory
     */
    public Language(String cdNewsCategory, String dsNewsCategory) {
        super();
        this.cdNewsCategory = cdNewsCategory;
        this.dsNewsCategory = dsNewsCategory;
    }

    public String getCdNewsCategory() {
        return cdNewsCategory;
    }

    public void setCdNewsCategory(String cdNewsCategory) {
        this.cdNewsCategory = cdNewsCategory;
    }

    public String getDsNewsCategory() {
        return dsNewsCategory;
    }

    public void setDsNewsCategory(String dsNewsCategory) {
        this.dsNewsCategory = dsNewsCategory;
    }

    @Override
    public String toString() {
        return this.dsNewsCategory;
    }
}