package com.project.centroid.newfeedapp.rest;

/**
 * @author Sooraj Soman on 11/1/2018
 */
public class ApiConstants {
    public static final String BASEURL = "http://centroidsolutions.in/15_news/api/";
    public static final String SHAREDPREF="MYPREF";
    public static final String USERTOKEN="TOKEN";
    public static final String USERLANG="lang";
    public static final String LOGINSTATE ="";
    public static  final  String  BEARER="Bearer ";
    public static  final  String  LANGUAGES="";


    public static  String TYPE  ;
}
