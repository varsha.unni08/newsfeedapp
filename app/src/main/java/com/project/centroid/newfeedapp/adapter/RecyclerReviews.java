package com.project.centroid.newfeedapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.project.centroid.newfeedapp.R;
import com.project.centroid.newfeedapp.model.ViewReviews;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecyclerReviews extends RecyclerView.Adapter<RecyclerReviews.ReviewHolder> {
    private final Context context;

    private List<ViewReviews> items;

    public RecyclerReviews(List<ViewReviews> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public ReviewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_review, parent, false);
        return new ReviewHolder(v);
    }

    @Override
    public void onBindViewHolder(ReviewHolder holder, int position) {
        ViewReviews item = items.get(position);
        if (item != null) {
            holder.newsTitleText.setText("" + item.getCdNews());
            holder.sectionNameText.setText("comment :" + item.getDsNewsReviews());
            if(item.getDsStarRating()!=null&&!TextUtils.isEmpty(item.getDsStarRating())){
                holder.rating.setRating(Float.parseFloat(item.getDsStarRating()));

            }
           // holder.datePublishedText.setText("rating :" + item.getDsStarRating());
        }
        //TODO Fill in your logic for binding the view.
    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    public class ReviewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.story_image)
        ImageView storyImage;
        @BindView(R.id.news_title_text)
        TextView newsTitleText;
        @BindView(R.id.section_name_text)
        TextView sectionNameText;
        @BindView(R.id.date_published_text)
        TextView datePublishedText;
        @BindView(R.id.story_card)
        CardView storyCard;

        @BindView(R.id.rating)
        RatingBar rating;
        public ReviewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}