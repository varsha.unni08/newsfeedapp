package com.project.centroid.newfeedapp.rest;

public class Utils {
 public  static  Integer sId;
 public  static  Integer pId;
 public  static  Integer dId;

    public static String checkLanguage(String id) {

        switch (id){

            case "2":return "en";
            case "4":return "hi";
            case "5":return "kn";
            case "6":return "ml";
            case "9":return "ta";
            default:return "en";
        }


    }
}
