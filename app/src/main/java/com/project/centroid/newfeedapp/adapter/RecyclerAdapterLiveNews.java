package com.project.centroid.newfeedapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.project.centroid.newfeedapp.LiveNewsActivity;
import com.project.centroid.newfeedapp.R;
import com.project.centroid.newfeedapp.model.LiveNewsItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecyclerAdapterLiveNews extends RecyclerView.Adapter<RecyclerAdapterLiveNews.ReviewHolder> {
    private final Context context;

    private List<LiveNewsItem> items;

    public RecyclerAdapterLiveNews(List<LiveNewsItem> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public ReviewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_video, parent, false);
        return new ReviewHolder(v);
    }

    @Override
    public void onBindViewHolder(ReviewHolder holder, final int position) {
        final LiveNewsItem item = items.get(position);
        if (item != null) {
            holder.newsTitleText.setText("" + item.getDsNewsHead());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!TextUtils.isEmpty(items.get(position).getVideo())) {
                        ((LiveNewsActivity) (context)).startVideoActivity(items.get(position).getVideo());
                    }
                }
            });

            // holder.datePublishedText.setText("rating :" + item.getDsStarRating());
        }


        //TODO Fill in your logic for binding the view.
    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    public class ReviewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.news_title_text)
        TextView newsTitleText;


        public ReviewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}