package com.project.centroid.newfeedapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.project.centroid.newfeedapp.application.MyApplication;
import com.project.centroid.newfeedapp.reciever.NetworkChangeReceiver;
import com.project.centroid.newfeedapp.rest.ApiConstants;
import com.project.centroid.newfeedapp.rest.RestService;
import com.project.centroid.newfeedapp.utils.LocaleHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity implements NetworkChangeReceiver.ConnectivityReceiverListener{

    @BindView(R.id.lEditEmail)
    EditText lEditEmail;
    @BindView(R.id.lTextEmail)
    TextInputLayout lTextEmail;
    @BindView(R.id.lEditPassword)
    EditText lEditPassword;
    @BindView(R.id.lTextPassword)
    TextInputLayout lTextPassword;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.btnLinkToRegisterScreen)
    Button btnLinkToRegisterScreen;
    private SharedPreferences sharedpreferences;
    private AwesomeValidation mAwesomeValidation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        sharedpreferences = this.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
        try{
            if(!TextUtils.isEmpty(sharedpreferences.getString(ApiConstants.USERLANG, ""))){
                String lan=sharedpreferences.getString(ApiConstants.USERLANG, "");
                LocaleHelper.setLocale(Login.this, lan);
            }
        }catch (Exception  e){
            e.printStackTrace();
        }
        if (!TextUtils.isEmpty(sharedpreferences.getString(ApiConstants.LOGINSTATE, ""))) {
            if (NetworkChangeReceiver.isConnected()) {
                startActivity(new Intent(Login.this, MainActivity.class));
                finish();
            } else {
                Snackbar.make(findViewById(android.R.id.content), "No internet connection.Please try after some time!", Snackbar.LENGTH_SHORT).show();

            }
        }
        validate();
    }



    @OnClick({R.id.btnLogin, R.id.btnLinkToRegisterScreen})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                if(mAwesomeValidation.validate()){
                    if (NetworkChangeReceiver.isConnected()) {
                      loginCall();
                    } else {
                        Snackbar.make(findViewById(android.R.id.content), "No internet connection.Please try after some time!", Snackbar.LENGTH_SHORT).show();

                    }

                }
                break;
            case R.id.btnLinkToRegisterScreen:
                startActivity(new Intent(getApplicationContext(),Registration.class));
                finish();
                break;
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().setConnectivityListener(this);

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
    private void validate() {
        mAwesomeValidation = new AwesomeValidation(ValidationStyle.COLORATION);
        Pattern pattern = Pattern.compile("^[6-9]\\d{9}$");
        mAwesomeValidation.addValidation(this, R.id.rEditEmail, "^[6-9]\\d{9}$", R.string.err_phone);
        mAwesomeValidation.addValidation(this, R.id.lEditPassword, RegexTemplate.NOT_EMPTY, R.string.err_password);

    }
    private void loginCall() {

        final ProgressDialog progressDialog = new ProgressDialog(this,
               ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();



        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<ResponseBody> callback = client.loginRequest("login", lEditEmail.getText().toString(), lEditPassword.getText().toString());
        callback.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.v("TAG", response.body().toString());
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    try {
                        JSONObject jsonObject= new JSONObject(response.body().string());
                        if(jsonObject.getString("message").equalsIgnoreCase("Login successfull")){

                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString(ApiConstants.USERTOKEN, jsonObject.getString("token"));
                            editor.putString(ApiConstants.LOGINSTATE, "true");
                            editor.apply();
                            editor.commit();
                            onLoginSuccess();

                        }else{

                             Snackbar.make(findViewById(android.R.id.content), "Login Failed.Please try again!", Snackbar.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }
    public void onLoginSuccess() {


        startActivity(new Intent(this, MainActivity.class));
        finish();


    }


}
