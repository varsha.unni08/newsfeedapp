package com.project.centroid.newfeedapp.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.project.centroid.newfeedapp.LinearLayoutManagerWrapper;
import com.project.centroid.newfeedapp.MainActivity;
import com.project.centroid.newfeedapp.R;
import com.project.centroid.newfeedapp.adapter.NewsListAdapter;
import com.project.centroid.newfeedapp.adapter.RecyclerAdapterNews;
import com.project.centroid.newfeedapp.model.NewsItem;
import com.project.centroid.newfeedapp.rest.ApiConstants;
import com.project.centroid.newfeedapp.rest.RestService;
import com.project.centroid.newfeedapp.utils.LocaleHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHealth extends Fragment {

   RecyclerView recyclerView;
    private SharedPreferences sharedpreferences;
    private List<NewsItem> newsModelList;
    private TabLayout tabLayout;
    private String categoryName;
    private RecyclerAdapterNews recyclerAdapter;
    private ProgressDialog progressDialog;
    private List<NewsItem> newsItems;
    private Integer page;

    private boolean allContactsLoaded=false;
    private int pageCount=1;

    public FragmentHealth() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View v=inflater.inflate(R.layout.fragment_blank, container, false);
       recyclerView =(RecyclerView)v.findViewById(R.id.reyclerview);
        sharedpreferences = getActivity().getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
    newsItems=new ArrayList<>();
       inflateRecycler(v);
        try{
            if(!TextUtils.isEmpty(sharedpreferences.getString(ApiConstants.USERLANG, ""))){
                String lan=sharedpreferences.getString(ApiConstants.USERLANG, "");
                LocaleHelper.setLocale(getActivity() ,lan);
            }}catch (Exception e){
            e.printStackTrace();
        }



        tabLayout=((MainActivity)getActivity()).tabLayout;
   /*    tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
           @Override
           public void onTabSelected(TabLayout.Tab tab) {

   

               categoryName=tab.getText().toString();
               Log.v("TAG",""+categoryName);
               if(categoryName.equalsIgnoreCase("sports")){
               categoryBYNews(categoryName);}else  if(categoryName.equalsIgnoreCase("politics")){
                   categoryBYNews(categoryName);
               }else  if(categoryName.equalsIgnoreCase("entertainment")){
                   categoryBYNews(categoryName);
               }else  if(categoryName.equalsIgnoreCase("technology")){
                   categoryBYNews(categoryName);
               }else  if(categoryName.equalsIgnoreCase("agriculture")){
                   categoryBYNews(categoryName);
               }else  if(categoryName.equalsIgnoreCase("health")){
                   categoryBYNews(categoryName);
               }
           }
           

           @Override
           public void onTabUnselected(TabLayout.Tab tab) {

           }

           @Override
           public void onTabReselected(TabLayout.Tab tab) {

           }
       });
*///      categoryName= tabLayout.getTabAt(tabLayout.getSelectedTabPosition()).getText().toString();
     // Log.v("TAG","name"+categoryName);
        //categoryBYNews();
        initRecyclerview("health");
       return v;
    }

    private void inflateRecycler(View v)
    {
        NewsListAdapter recyclerAdapter=new NewsListAdapter(getActivity(),newsItems,recyclerView);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        //recyclerView.setLayoutAnimation(new De);
        recyclerView.setAdapter(recyclerAdapter);

    }





    private void initRecyclerview(final String type) {

        newsItems = new ArrayList<>();
        LinearLayoutManager mLayoutManager = new LinearLayoutManagerWrapper(getActivity(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(mLayoutManager);





        RestService.RestApiInterface client = RestService.getClient();

        Call<List<NewsItem>> callBack = client.getNewsByCategory(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), type, 1);

        callBack.enqueue(new Callback<List<NewsItem>>() {
            @Override
            public void onResponse(Call<List<NewsItem>> call, Response<List<NewsItem>> response) {

                try {

                    if (response.body() != null) {

                        List<NewsItem> posts =response.body();

                        if (posts.size() > 0) {
                            newsItems.addAll(posts);
                            // recyclerAdapter.notifyDataSetChanged();
                            recyclerAdapter = new RecyclerAdapterNews(getActivity(), newsItems, recyclerView);
                            recyclerView.setAdapter(recyclerAdapter);
                            if (!allContactsLoaded) {
                                try{
                                    loadMore(type);}catch (Exception e){
                                    e.printStackTrace();
                                }

                            }

                        } else {


                        }

                    } else {


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<NewsItem>> call, Throwable t) {

            }
        });

    }

    //setSearchView();


    private void loadMore(final String mtype) {
        recyclerAdapter.setOnLoadMoreListener(new RecyclerAdapterNews.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.v("TAG", "loadmore");

                newsItems.add(null);
                recyclerView.post(new Runnable() {
                    public void run() {
                        recyclerAdapter.notifyItemInserted(newsItems.size() - 1);
                    }
                });
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //remove progress item
                     /*   if(newsItems.size()>0){
                        ////newsItems.remove(newsItems.size() - 1);
                        recyclerAdapter.notifyItemInserted(newsItems.size());}
                     */   //add items one by one
/*                        for (int i = 0; i < 15; i++) {
                            myDataset.add("Item" + (myDataset.size() + 1));
                            mAdapter.notifyItemInserted(myDataset.size());

                        }*/
                        Log.v("TAG", "loading");
                        if (!allContactsLoaded) {

                            pageCount++;

                            try {
                                getMoreData(pageCount, mtype);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            //recyclerAdapter.setLoaded();

                        }
                        //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
                    }
                }, 2000);
                //System.out.println("load");
            }
        });

    }

    private void getMoreData(final int page, String memtype) {
        RestService.RestApiInterface client = RestService.getClient();
        Call< List<NewsItem> > callBack = client.getNewsByCategory(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), memtype, page);

        callBack.enqueue(new Callback<List<NewsItem>>() {
            @Override
            public void onResponse(Call<List<NewsItem>> call, Response<List<NewsItem>> response) {

                try {

                    if (!response.body().isEmpty()) {
                        if (response.body() != null) {


                            List<NewsItem> posts = response.body();
                            if (posts.size() > 0) {
                                Log.v("TAG", "newloading");
                                recyclerAdapter.setLoaded();
                                newsItems.remove(newsItems.size() - 1);
                                newsItems.addAll(posts);
                                recyclerAdapter.notifyItemInserted(newsItems.size());


                            } else {

                                Log.v("TAG", "loadingfalse");
                                recyclerAdapter.setLoadedFalse();
                                pageCount = 1;
                                allContactsLoaded = true;
                                newsItems.remove(newsItems.size() - 1);
                                recyclerAdapter.notifyDataSetChanged();


                            }
                        } else {
                            Log.v("TAG", "loadingend");

                            allContactsLoaded = true;
                            newsItems.remove(newsItems.size() - 1);
                            recyclerAdapter.setLoadedFalse();
                            recyclerAdapter.notifyDataSetChanged();


                        }}else{
                        allContactsLoaded = true;
                        newsItems.remove(newsItems.size() - 1);
                        recyclerAdapter.setLoadedFalse();
                        recyclerAdapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<NewsItem>> call, Throwable t) {

            }
        });


    }



}
