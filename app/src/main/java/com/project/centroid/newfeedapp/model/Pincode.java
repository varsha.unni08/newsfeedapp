
        package com.project.centroid.newfeedapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pincode {

    @SerializedName("cd_pincode")
    @Expose
    private String cdPincode;
    @SerializedName("ds_pincode")
    @Expose
    private String dsPincode;
    @SerializedName("cd_state")
    @Expose
    private String cdState;
    @SerializedName("cd_district")
    @Expose
    private String cdDistrict;

    public String getCdPincode() {
        return cdPincode;
    }

    public void setCdPincode(String cdPincode) {
        this.cdPincode = cdPincode;
    }

    public String getCdState() {
        return cdState;
    }

    public void setCdState(String cdState) {
        this.cdState = cdState;
    }

    public String getCdDistrict() {
        return cdDistrict;
    }

    public void setCdDistrict(String cdDistrict) {
        this.cdDistrict = cdDistrict;
    }
    @Override
    public String toString() {
        return this.dsPincode;
    }

    public String getDsPincode() {
        return dsPincode;
    }

    public void setDsPincode(String dsPincode) {
        this.dsPincode = dsPincode;
    }
}