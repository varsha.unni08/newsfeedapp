package com.project.centroid.newfeedapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class State {

    @SerializedName("cd_state")
    @Expose
    private String cdState;
    @SerializedName("cd_country")
    @Expose
    private String cdCountry;
    @SerializedName("ds_state")
    @Expose
    private String dsState;

    public String getCdState() {
        return cdState;
    }

    public void setCdState(String cdState) {
        this.cdState = cdState;
    }

    public String getCdCountry() {
        return cdCountry;
    }

    public void setCdCountry(String cdCountry) {
        this.cdCountry = cdCountry;
    }

    public String getDsState() {
        return dsState;
    }

    public void setDsState(String dsState) {
        this.dsState = dsState;
    }

    @Override
    public String toString() {
        return this.dsState.toLowerCase();
    }
}