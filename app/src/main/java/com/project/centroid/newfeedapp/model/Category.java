package com.project.centroid.newfeedapp.model; ;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {

    @SerializedName("cd_news_category")
    @Expose
    private String cdNewsCategory;
    @SerializedName("ds_news_category")
    @Expose
    private String dsNewsCategory;

    /**
     * No args constructor for use in serialization
     *
     */
    public Category() {
    }

    /**
     *
     * @param cdNewsCategory
     * @param dsNewsCategory
     */
    public Category(String cdNewsCategory, String dsNewsCategory) {
        super();
        this.cdNewsCategory = cdNewsCategory;
        this.dsNewsCategory = dsNewsCategory;
    }

    public String getCdNewsCategory() {
        return cdNewsCategory;
    }

    public void setCdNewsCategory(String cdNewsCategory) {
        this.cdNewsCategory = cdNewsCategory;
    }

    public String getDsNewsCategory() {
        return dsNewsCategory;
    }

    public void setDsNewsCategory(String dsNewsCategory) {
        this.dsNewsCategory = dsNewsCategory;
    }

}