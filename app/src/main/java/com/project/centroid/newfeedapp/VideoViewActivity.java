package com.project.centroid.newfeedapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.VideoView;

import com.google.gson.Gson;
import com.project.centroid.newfeedapp.model.NewsItem;
import com.project.centroid.newfeedapp.rest.ApiConstants;
import com.project.centroid.newfeedapp.utils.LocaleHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoViewActivity extends AppCompatActivity {

    @BindView(R.id.video)
    VideoView video;

    String video_url;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      SharedPreferences sharedpreferences = this.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
        try{
            if(!TextUtils.isEmpty(sharedpreferences.getString(ApiConstants.USERLANG, ""))){
                String lan=sharedpreferences.getString(ApiConstants.USERLANG, "");
                LocaleHelper.setLocale(VideoViewActivity.this, lan);
            }}catch (Exception e){
            e.printStackTrace();
        }
        setContentView(R.layout.activity_video_view);
        ButterKnife.bind(this);
        pd = new ProgressDialog(this);
        pd.setMessage("Buffering video please wait...");
        pd.show();
        try {


            if (getIntent() != null) {
                String comeFrom = getIntent().getStringExtra("comefrom");

                if (comeFrom != null && !TextUtils.isEmpty(comeFrom)) {
                    if (comeFrom.equalsIgnoreCase("1")) {
                        NewsItem news = new Gson().fromJson(getIntent().getStringExtra("DATA"), NewsItem.class);
                        video_url = news.getDsVideo();
                    } else {
                        String url = getIntent().getStringExtra("url");
                        video_url = url;
                    }
                }
            }
            Uri uri = Uri.parse(video_url);
            video.setVideoURI(uri);
            video.start();

            video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    //close the progress dialog when buffering is done
                    pd.dismiss();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:


                Intent returnIntent = getIntent();

                setResult(Activity.RESULT_OK, returnIntent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
