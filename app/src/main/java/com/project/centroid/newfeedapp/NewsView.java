package com.project.centroid.newfeedapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;

import com.project.centroid.newfeedapp.adapter.NewsRecyclerAdapter;
import com.project.centroid.newfeedapp.model.NewsItem;
import com.project.centroid.newfeedapp.rest.ApiConstants;
import com.project.centroid.newfeedapp.rest.RestService;
import com.project.centroid.newfeedapp.utils.LocaleHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsView extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private List<NewsItem> newsModelList;
    private NewsRecyclerAdapter recyclerAdapter;
    private SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{
            if(!TextUtils.isEmpty(sharedpreferences.getString(ApiConstants.USERLANG, ""))){
                String lan=sharedpreferences.getString(ApiConstants.USERLANG, "");
                LocaleHelper.setLocale(NewsView.this, lan);
            }}catch (Exception e){
            e.printStackTrace();
        }
        setContentView(R.layout.activity_news_view);
        ButterKnife.bind(this);

        sharedpreferences = this.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if(ApiConstants.TYPE.equalsIgnoreCase("latest")){
        categoryBYNews(ApiConstants.TYPE);}else{
         //  categoryBYAll(ApiConstants.TYPE);
        }
    }


    private void categoryBYNews(String cName) {
        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<NewsItem>> callback = client.getLatestNews(ApiConstants.BEARER+sharedpreferences.getString(ApiConstants.USERTOKEN,""),1);
        callback.enqueue(new Callback<List<NewsItem>>() {
            @Override
            public void onResponse(Call<List<NewsItem>> call, Response<List<NewsItem>> response) {

                Log.v("TAG", response.body().toString());
                if (response.isSuccessful()) {

                    try {
                        newsModelList=new ArrayList<>();

                        newsModelList.addAll(response.body());

                        recyclerAdapter=new NewsRecyclerAdapter(NewsView.this,newsModelList);
                        //recyclerView.setLayoutAnimation(new De);
                        recyclerView.setAdapter(recyclerAdapter);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<List<NewsItem>> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }
    /*private void categoryBYAll(String cName) {
        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<NewsItem>> callback = client.getAllNews(ApiConstants.BEARER+sharedpreferences.getString(ApiConstants.USERTOKEN,""),cName);
        callback.enqueue(new Callback<List<NewsItem>>() {
            @Override
            public void onResponse(Call<List<NewsItem>> call, Response<List<NewsItem>> response) {


                if (response.isSuccessful()) {

                    try {
                        Log.v("TAG", response.body().toString());
                        newsModelList=new ArrayList<>();
                        newsModelList.addAll(response.body());
                        recyclerAdapter=new NewsRecyclerAdapter(NewsView.this,newsModelList);
                        //recyclerView.setLayoutAnimation(new De);
                        recyclerView.setAdapter(recyclerAdapter);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<List<NewsItem>> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }
*/

}
