package com.project.centroid.newfeedapp.rest;

import android.support.annotation.NonNull;

import com.project.centroid.newfeedapp.model.Category;
import com.project.centroid.newfeedapp.model.District;
import com.project.centroid.newfeedapp.model.Language;
import com.project.centroid.newfeedapp.model.LiveNewsItem;
import com.project.centroid.newfeedapp.model.NewsItem;
import com.project.centroid.newfeedapp.model.Pincode;
import com.project.centroid.newfeedapp.model.ReviewItem;
import com.project.centroid.newfeedapp.model.State;
import com.project.centroid.newfeedapp.model.ViewReviews;
import com.project.centroid.newfeedapp.model.VillageModel;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


public class
RestService {
    private static RestApiInterface RestApiInterface;


    public static RestApiInterface getClient() {

        if (RestApiInterface == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            // set your desired log level
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.interceptors().add(logging);

            OkHttpClient httpClient = builder.build();

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(ApiConstants.BASEURL)
                    .addConverterFactory(new NullOnEmptyConverterFactory())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .build();

            RestApiInterface = client.create(RestApiInterface.class);
        }
        return RestApiInterface;
    }




    public interface RestApiInterface {

        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("appUser.php")
        Call<ResponseBody> loginRequest(@Query("apicall") String callType, @Field("phone") String phone, @Field("password") String password);

        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("appUser.php")
        Call<ResponseBody> registration(@Query("apicall") String callType, @Field("name") String name, @Field("email") String email, @Field("phone") String phone, @Field("location") String location, @Field("pincode") String pincode, @Field("language") String language, @Field("username") String username, @Field("password") String password);


        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("addReview.php")
        Call<ResponseBody> addReview(@Header("Authorization") String bearerToken, @Field("cd_news") String id, @Field("ds_news_reviews") String review, @Field("ds_star_rating") Integer rating);


        @NonNull
        @Headers({"Accept: application/json"})
        @GET("master/featuredprofiles/{type}/{page}")
        Call<List<ResponseBody>> getFeaturedUsers(@Header("Authorization") String x_auth_token, @Path("type") String type, @Path("page") int page);

        @NonNull
        @Headers({"Accept: application/json"})
        @GET("newsCategories.php")
        Call<List<Category>> getNewsCategory(@Header("Authorization") String bearerToken);


        @Headers({"Accept: application/json"})
        @GET("latestNews.php")
        Call<List<NewsItem>> getLatestNews(@Header("Authorization") String bearerToken, @Query("currentpage") Integer pageNo);

        @Headers({"Accept: application/json"})
        @GET("newsByCategory.php")
        Call<List<NewsItem>> getNewsByCategory(@Header("Authorization") String bearerToken, @Query("apicall") String apikey, @Query("currentpage") Integer pageNo);

        @Headers({"Accept: application/json"})
        @GET("allNews.php")
        Call<List<NewsItem>> getAllNews(@Header("Authorization") String bearerToken, @Query("state") String state, @Query("district") String district, @Query("currentpage") Integer pageNo);

      /*  @Headers({"Accept: application/json"})
        @POST("searchNews.php")
        Call<List<NewsItem>> searchNews(@Header("Authorization") String bearerToken, @Query("search") String key);
     */   @Headers({"Accept: application/json"})
        @POST("searchNews.php")
        Call<List<NewsItem>> searchNews(@Header("Authorization") String bearerToken, @Query("dt_record") String key);

        @Headers({"Accept: application/json"})
        @GET("singleNewsView.php")
        Call<ResponseBody> getSingleNews(@Header("Authorization") String bearerToken, @Query("api_key") String apikey);

        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("switchPincode.php")
        Call<ResponseBody> switchPinCode(@Header("Authorization") String bearerToken, @Field("ds_pincode") String pincode);

        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("switchLanguage.php")
        Call<ResponseBody> switchLanguage(@Header("Authorization") String bearerToken, @Field("tmp_cd_news_language") String lang);

        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("viewNewsComments.php")
        Call<List<ViewReviews>> getReviews(@Header("Authorization") String bearerToken, @Field("cd_news") String news);

        @Headers({"Accept: application/json"})
        @GET("yourReviews.php")
        Call<List<ReviewItem>> getReviews(@Header("Authorization") String bearerToken);

        @Headers({"Accept: application/json"})
        @GET("languages.php")
        Call<List<Language>> getLanguages();

        @Headers({"Accept: application/json"})
        @GET("viewStateDistrictPincode.php")
        Call<List<State>> getAllStates(@Header("Authorization") String bearerToken, @Query("apicall") String apicall);

        @Headers({"Accept: application/json"})
        @GET("viewStateDistrictPincode.php")
        Call<List<District>> getAllDistrict(@Header("Authorization") String bearerToken, @Query("apicall") String district, @Query("state") String state);

        @Headers({"Accept: application/json"})
        @GET("viewStateDistrictPincode.php")
        Call<List<Pincode>> getAllPincode(@Header("Authorization") String bearerToken, @Query("apicall") String apiCall, @Query("district") String district);

        @Headers({"Accept: application/json"})
        @GET("viewStateDistrictPincode.php")
        Call<List<VillageModel>> getAllVillage(@Header("Authorization") String bearerToken, @Query("apicall") String apiCall, @Query("pincode") String pinCode);
        @Headers({"Accept: application/json"})
        @GET("get_live.php")
        Call<List<LiveNewsItem>> getLiveNews(@Header("Authorization") String bearerToken);
    }

}
