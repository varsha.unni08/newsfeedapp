package com.project.centroid.newfeedapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LiveNewsItem {

    @SerializedName("ds_news_head")
    @Expose
    private String dsNewsHead;

    @SerializedName("video")
    @Expose
    private String video;

    public void setDsNewsHead(String dsNewsHead) {
        this.dsNewsHead = dsNewsHead;
    }

    public String getDsNewsHead() {
        return dsNewsHead;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getVideo() {
        return video;
    }
}