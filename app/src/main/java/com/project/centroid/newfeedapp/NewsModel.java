package com.project.centroid.newfeedapp;

import android.graphics.Bitmap;

public class NewsModel {
    /** News title */
    private String mTitle;

    /** News section */
    private String mSection;

    /** Date when news was published */
    private String mDate;

    /** Url for the news story */
    private String mStoryUrl;

    /** Url for thumbnail of news story */
    private String mThumbnailUrl;
    private int  mTDrawable;

    /** Thumbnail bitmap of news story */
    private Bitmap mThumbnail;

    String  rating;

    public NewsModel() {
    }

    public NewsModel(String mTitle, String mSection, String mDate, String mStoryUrl, String mThumbnailUrl, Bitmap mThumbnail) {
        this.mTitle = mTitle;
        this.mSection = mSection;
        this.mDate = mDate;
        this.mStoryUrl = mStoryUrl;
        this.mThumbnailUrl = mThumbnailUrl;
        this.mThumbnail = mThumbnail;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmSection() {
        return mSection;
    }

    public void setmSection(String mSection) {
        this.mSection = mSection;
    }

    public String getmDate() {
        return mDate;
    }

    public void setmDate(String mDate) {
        this.mDate = mDate;
    }

    public String getmStoryUrl() {
        return mStoryUrl;
    }

    public void setmStoryUrl(String mStoryUrl) {
        this.mStoryUrl = mStoryUrl;
    }

    public String getmThumbnailUrl() {
        return mThumbnailUrl;
    }

    public void setmThumbnailUrl(String mThumbnailUrl) {
        this.mThumbnailUrl = mThumbnailUrl;
    }

    public Bitmap getmThumbnail() {
        return mThumbnail;
    }

    public void setmThumbnail(Bitmap mThumbnail) {
        this.mThumbnail = mThumbnail;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public int getmTDrawable() {
        return mTDrawable;
    }

    public void setmTDrawable(int mTDrawable) {
        this.mTDrawable = mTDrawable;
    }
}
