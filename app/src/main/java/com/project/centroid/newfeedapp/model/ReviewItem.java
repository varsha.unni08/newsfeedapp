package com.project.centroid.newfeedapp.model; ;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReviewItem {

    @SerializedName("cd_news")
    @Expose
    private String cdNews;
    @SerializedName("ds_news_head")
    @Expose
    private String dsNewsHead;
    @SerializedName("ds_news_content")
    @Expose
    private String dsNewsContent;
    @SerializedName("ds_image")
    @Expose
    private String dsImage;
    @SerializedName("ds_video")
    @Expose
    private String dsVideo;
    @SerializedName("ds_news_reviews")
    @Expose
    private String dsNewsReviews;
    @SerializedName("ds_star_rating")
    @Expose
    private String dsStarRating;

    /**
     * No args constructor for use in serialization
     *
     */
    public ReviewItem() {
    }

    /**
     *
     * @param dsImage
     * @param dsNewsReviews
     * @param dsVideo
     * @param dsNewsContent
     * @param dsStarRating
     * @param cdNews
     * @param dsNewsHead
     */
    public ReviewItem(String cdNews, String dsNewsHead, String dsNewsContent, String dsImage, String dsVideo, String dsNewsReviews, String dsStarRating) {
        super();
        this.cdNews = cdNews;
        this.dsNewsHead = dsNewsHead;
        this.dsNewsContent = dsNewsContent;
        this.dsImage = dsImage;
        this.dsVideo = dsVideo;
        this.dsNewsReviews = dsNewsReviews;
        this.dsStarRating = dsStarRating;
    }

    public String getCdNews() {
        return cdNews;
    }

    public void setCdNews(String cdNews) {
        this.cdNews = cdNews;
    }

    public String getDsNewsHead() {
        return dsNewsHead;
    }

    public void setDsNewsHead(String dsNewsHead) {
        this.dsNewsHead = dsNewsHead;
    }

    public String getDsNewsContent() {
        return dsNewsContent;
    }

    public void setDsNewsContent(String dsNewsContent) {
        this.dsNewsContent = dsNewsContent;
    }

    public String getDsImage() {
        return dsImage;
    }

    public void setDsImage(String dsImage) {
        this.dsImage = dsImage;
    }

    public String getDsVideo() {
        return dsVideo;
    }

    public void setDsVideo(String dsVideo) {
        this.dsVideo = dsVideo;
    }

    public String getDsNewsReviews() {
        return dsNewsReviews;
    }

    public void setDsNewsReviews(String dsNewsReviews) {
        this.dsNewsReviews = dsNewsReviews;
    }

    public String getDsStarRating() {
        return dsStarRating;
    }

    public void setDsStarRating(String dsStarRating) {
        this.dsStarRating = dsStarRating;
    }

}