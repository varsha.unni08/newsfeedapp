package com.project.centroid.newfeedapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.project.centroid.newfeedapp.adapter.NewsRecyclerAdapter;
import com.project.centroid.newfeedapp.application.MyApplication;
import com.project.centroid.newfeedapp.model.Language;
import com.project.centroid.newfeedapp.model.NewsItem;
import com.project.centroid.newfeedapp.reciever.NetworkChangeReceiver;
import com.project.centroid.newfeedapp.rest.ApiConstants;
import com.project.centroid.newfeedapp.rest.RestService;
import com.project.centroid.newfeedapp.rest.Utils;
import com.project.centroid.newfeedapp.utils.LocaleHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registration extends AppCompatActivity implements NetworkChangeReceiver.ConnectivityReceiverListener {


    @BindView(R.id.btnRegister)
    Button btnRegister;
    @BindView(R.id.btnLinkToLoginScreen)
    Button btnLinkToLoginScreen;
    @BindView(R.id.rName)
    EditText rName;
    @BindView(R.id.rTextName)
    TextInputLayout rTextName;
    @BindView(R.id.rEditEmail)
    EditText rEditEmail;
    @BindView(R.id.rTextEmail)
    TextInputLayout rTextEmail;
    @BindView(R.id.rEditPhone)
    EditText rEditPhone;
    @BindView(R.id.rTextPhone)
    TextInputLayout rTextPhone;
    @BindView(R.id.rEditlocation)
    EditText rEditlocation;
    @BindView(R.id.rTextlocation)
    TextInputLayout rTextlocation;
    @BindView(R.id.rTextlanguage)
    AppCompatTextView rTextlanguage;
    @BindView(R.id.rSpinner)
    AppCompatSpinner rSpinner;
    @BindView(R.id.rEditPincode)
    EditText rEditPincode;
    @BindView(R.id.rTextPincode)
    TextInputLayout rTextPincode;
    @BindView(R.id.rEditUserName)
    EditText rEditUserName;
    @BindView(R.id.rTextUserName)
    TextInputLayout rTextUserName;
    @BindView(R.id.rEditPassword)
    EditText rEditPassword;
    @BindView(R.id.rTextPassword)
    TextInputLayout rTextPassword;
    private AwesomeValidation mAwesomeValidation;
    private SharedPreferences sharedpreferences;
    private ArrayList<Language> newsModelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        sharedpreferences = this.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
        categoryBYAll();
        checkValid();


    }

    private void checkValid() {
        mAwesomeValidation = new AwesomeValidation(ValidationStyle.COLORATION);
        Pattern pattern = Pattern.compile("^[6-9]\\d{9}$");
        mAwesomeValidation.addValidation(this, R.id.rName, "[a-zA-Z_\\s]{3,}", R.string.err_name);
        mAwesomeValidation.addValidation(this, R.id.rEditUserName, RegexTemplate.NOT_EMPTY, R.string.err_uname);
        // mAwesomeValidation.addValidation(getActivity(), R.id.etDob, RegexTemplate.NOT_EMPTY, R.string.err_dob);
        mAwesomeValidation.addValidation(this, R.id.rEditPhone, "^[6-9]\\d{9}$", R.string.err_phone);
        mAwesomeValidation.addValidation(this, R.id.rEditPincode, "^\\d{6}$", R.string.err_pincode);
        mAwesomeValidation.addValidation(this, R.id.rEditlocation, RegexTemplate.NOT_EMPTY, R.string.err_location);
        mAwesomeValidation.addValidation(this, R.id.rEditPassword, RegexTemplate.NOT_EMPTY, R.string.err_password);
        mAwesomeValidation.addValidation(this, R.id.rEditEmail, Patterns.EMAIL_ADDRESS, R.string.err_email);

    }

    @OnClick({R.id.btnRegister, R.id.btnLinkToLoginScreen})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnRegister:

                if (mAwesomeValidation.validate()&&rSpinner.getSelectedItemPosition()!=0) {
                    if (NetworkChangeReceiver.isConnected()) {
                        postRequest();
                    } else {
                        Snackbar.make(findViewById(android.R.id.content), "No internet connection.Please try after some time!", Snackbar.LENGTH_SHORT).show();
                    }
                }
                else {
                    Snackbar.make(findViewById(android.R.id.content), "Please Select language!", Snackbar.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnLinkToLoginScreen:
                startActivity(new Intent(getApplicationContext(), Login.class));
                finish();
                break;
        }
    }

    private void postRequest() {
        final ProgressDialog progressDialog = new ProgressDialog(this,
                ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Registering...");
        progressDialog.show();


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<ResponseBody> callback = client.registration("signup", rName.getText().toString(),rEditEmail.getText().toString(),
                rEditPhone.getText().toString(),
                rEditlocation.getText().toString(),
                rEditPincode.getText().toString(),
                rSpinner.getSelectedItem().toString(),
                rEditUserName.getText().toString(),
                rEditPassword.getText().toString());
        callback.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.v("TAG", response.body().toString());
                progressDialog.dismiss();
                progressDialog.hide();
                if (response.isSuccessful()) {

                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.getString("status_message").equalsIgnoreCase("User Registered Successfully.")) {

                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString(ApiConstants.USERTOKEN, jsonObject.getString("token"));
                            editor.putString(ApiConstants.LOGINSTATE, "true");
                            editor.apply();
                            editor.commit();
                            if(rSpinner!=null&&rSpinner.getCount()>0){
                                String id=((Language)rSpinner.getSelectedItem()).getCdNewsCategory();
                                String lan=  Utils.checkLanguage(id);

                                //LocaleHelper.setLocale(Registration.this, lan);

                                editor.putString(ApiConstants.USERLANG, lan);

                                editor.apply();
                                editor.commit();

                                //It is required to recreate the activity to reflect the change in UI.
                                recreate();
                            }
                           // onLoginSuccess();

                        } else {

                            Snackbar.make(findViewById(android.R.id.content), "Something went  wrong.Please try after some time!", Snackbar.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                progressDialog.hide();
                t.printStackTrace();
            }
        });
    }

    public void onLoginSuccess() {


        startActivity(new Intent(this, Login.class));
        finish();


    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().setConnectivityListener(this);

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
    private void categoryBYAll() {
        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<Language>> callback = client.getLanguages();
        callback.enqueue(new Callback<List<Language>>() {
            @Override
            public void onResponse(Call<List<Language>> call, Response<List<Language>> response) {

                Log.v("TAG", response.body().toString());
                if (response.isSuccessful()) {

                    try {

                        List<String>list=new ArrayList<>();
                        list.add(0,"Select");
                        for(int i=0;i<response.body().size();i++){
                            list.add(response.body().get(i).getDsNewsCategory());

                        }
                        ArrayAdapter<String> adapter =
                                new ArrayAdapter<String>(getApplicationContext(),  android.R.layout.simple_spinner_dropdown_item, list);
                        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

                        rSpinner.setAdapter(adapter);
                        ((TextView)rSpinner.getSelectedItem()).setTextColor(getResources().getColor(R.color.colorPrimaryText));


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<List<Language>> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }
}
