package com.project.centroid.newfeedapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;

import com.project.centroid.newfeedapp.adapter.ReviewRecyclerAdapter;
import com.project.centroid.newfeedapp.model.ReviewItem;
import com.project.centroid.newfeedapp.rest.ApiConstants;
import com.project.centroid.newfeedapp.rest.RestService;
import com.project.centroid.newfeedapp.utils.LocaleHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewCommentsActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private SharedPreferences sharedpreferences;
    private List<ReviewItem> newsModelList;
    private ReviewRecyclerAdapter recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedpreferences = this.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
        try{
            if(!TextUtils.isEmpty(sharedpreferences.getString(ApiConstants.USERLANG, ""))){
                String lan=sharedpreferences.getString(ApiConstants.USERLANG, "");
                LocaleHelper.setLocale(ViewCommentsActivity.this, lan);
            }
        }catch (Exception  e){
            e.printStackTrace();
        }

        setContentView(R.layout.activity_view_review);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getReviews();
    }

    private void getReviews() {
        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<ReviewItem>> callback = client.getReviews(ApiConstants.BEARER+sharedpreferences.getString(ApiConstants.USERTOKEN,""));
        callback.enqueue(new Callback<List<ReviewItem>>() {
            @Override
            public void onResponse(Call<List<ReviewItem>> call, Response<List<ReviewItem>> response) {


                if (response.isSuccessful()&&response!=null) {

                    try {

                        Log.v("TAG", response.body().toString());
                        newsModelList=new ArrayList<>();
                        newsModelList.addAll(response.body());
                        recyclerAdapter=new ReviewRecyclerAdapter(ViewCommentsActivity.this,newsModelList);
                        //recyclerView.setLayoutAnimation(new De);
                        recyclerView.setAdapter(recyclerAdapter);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<List<ReviewItem>> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }

}
