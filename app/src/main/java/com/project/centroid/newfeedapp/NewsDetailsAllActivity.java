package com.project.centroid.newfeedapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.project.centroid.newfeedapp.adapter.RecyclerReviews;
import com.project.centroid.newfeedapp.model.NewsItem;
import com.project.centroid.newfeedapp.model.ViewReviews;
import com.project.centroid.newfeedapp.rest.ApiConstants;
import com.project.centroid.newfeedapp.rest.RestService;
import com.project.centroid.newfeedapp.utils.LocaleHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsDetailsAllActivity extends AppCompatActivity {

    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.title2)
    TextView title2;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    @BindView(R.id.textView2)
    TextView textView2;
    @BindView(R.id.button)
    Button button;
    @BindView(R.id.button1)
    Button button1;
    private NewsItem newsModel;
    private SharedPreferences sharedpreferences;
    private RecyclerView recycler;
    private AlertDialog dialog;
    private ArrayList<ViewReviews> newsModelList;
    private RecyclerReviews recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            sharedpreferences = this.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
            newsModel = new Gson().fromJson(getIntent().getStringExtra("DATA"), NewsItem.class);
            Glide.with(this).load(newsModel.getDsImage()).into(imageView);
            title.setText(newsModel.getDsNewsHead());
            title2.setText(newsModel.getDsNewsContent());
            try{
                if(!TextUtils.isEmpty(sharedpreferences.getString(ApiConstants.USERLANG, ""))){
                    String lan=sharedpreferences.getString(ApiConstants.USERLANG, "");
                    LocaleHelper.setLocale(NewsDetailsAllActivity.this, lan);
                }
                setContentView(R.layout.activity_news_details);
                ButterKnife.bind(this);

            }catch (Exception  e){
                e.printStackTrace();
            }

            if(newsModel.getTotalReviews()!=null&&Integer.parseInt(newsModel.getTotalReviews())>=0){

            textView2.setText(""+newsModel.getTotalReviews());
            }else{
                textView2.setVisibility(View.INVISIBLE);
            }
            if(newsModel.getNewsRating()!=null){
            ratingBar.setRating(Float.parseFloat(newsModel.getNewsRating()));}

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @OnClick({R.id.button, R.id.button1,R.id.textView2})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button:
                if(!TextUtils.isEmpty(newsModel.getDsVideo())){
                    Intent intent=new Intent(getApplicationContext(),VideoViewActivity.class);
                    intent.putExtra("comefrom","1");
                    intent.putExtra("DATA",new Gson().toJson(newsModel));
                    startActivityForResult(intent,11);
            }else{
                    button.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.button1:
                 showDialogView();
                break;
                case R.id.textView2:
                   if( Integer.parseInt(newsModel.getTotalReviews())>0){
                 showFileChooserDialogCustom();}else{
                       Toast.makeText(getApplicationContext(),"No comments present",Toast.LENGTH_LONG).show();
                   }
                break;
        }
    }

    private void showDialogView() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.layout_ratingview, null);
        final RatingBar ratingBar=(RatingBar)dialogLayout.findViewById(R.id.ratingBar1) ;
        final EditText comment=(EditText)dialogLayout.findViewById(R.id.comments);

        builder.setPositiveButton("submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(ratingBar.getRating()>0.0&& !TextUtils.isEmpty(comment.getText().toString()))
                {
                    postServer(ratingBar.getRating(),comment.getText().toString());
                }
            }
        });builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        builder.setView(dialogLayout);
        builder.setCancelable(false);
        builder.show();
    }

    private void postServer(final  float rating, String s) {


            final ProgressDialog progressDialog = new ProgressDialog(this,
                    ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("please wait...");
            progressDialog.show();



            // TODO: Implement your own authentication logic here.
            RestService.RestApiInterface client = RestService.getClient();

            Call<ResponseBody> callback = client.addReview(ApiConstants.BEARER+sharedpreferences.getString(ApiConstants.USERTOKEN,""),newsModel.getCdNews(),s,((int)rating));
            callback.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                    if (response.isSuccessful()) {
                        progressDialog.dismiss();
                        progressDialog.hide();
                        try {

                            String responseBody = response.body().string();
                            if (responseBody.trim().charAt(0) == '{') {

                            } else if (responseBody.trim().charAt(0) == '[') {
                                JSONArray json= new JSONArray(responseBody);
                                JSONObject jsonObject=json.getJSONObject(0);
                                if(jsonObject.getString("status").equalsIgnoreCase("1")){

                                    Snackbar.make(findViewById(android.R.id.content), "Comment added auccessfully!", Snackbar.LENGTH_SHORT).show();


                                }else{
                                    Snackbar.make(findViewById(android.R.id.content), "You have already added comment. ", Snackbar.LENGTH_SHORT).show();

                                    // Snackbar.make(findViewById(android.R.id.content), "No internet connection.Please try after some time!", Snackbar.LENGTH_SHORT).show();

                                }
                            }
                           /* Log.v("TAG", response.body().toString());
                            JSONArray json= new JSONArray(response.body().string());
                            JSONObject jsonObject=json.getJSONObject(0);
                            if(jsonObject.getString("Message").equalsIgnoreCase("Review Added.")){

                                Snackbar.make(findViewById(android.R.id.content), "Review Added Successfully!", Snackbar.LENGTH_SHORT).show();


                            }else{
                                Snackbar.make(findViewById(android.R.id.content), "Something went wrong.Plese try after sometime!", Snackbar.LENGTH_SHORT).show();

                                // Snackbar.make(findViewById(android.R.id.content), "No internet connection.Please try after some time!", Snackbar.LENGTH_SHORT).show();

                            }*/


                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }


                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    t.printStackTrace();
                }
            });




    }


    private void showFileChooserDialogCustom() {
        final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) this
                .findViewById(R.id.parent)).getChildAt(0);
        View view = getLayoutInflater().inflate(R.layout.layout_dialog, viewGroup, false);
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Comments");
        recycler = (RecyclerView) view.findViewById(R.id.recyclerView);
        getReviews(recycler);
        pictureDialog.setView(view);
        dialog = pictureDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(lp);


    }

    private void getReviews(final RecyclerView recycler) {
        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<ViewReviews>> callback = client.getReviews(ApiConstants.BEARER+sharedpreferences.getString(ApiConstants.USERTOKEN,""),newsModel.getCdNews());
        callback.enqueue(new Callback<List<ViewReviews>>() {
            @Override
            public void onResponse(Call<List<ViewReviews>> call, Response<List<ViewReviews>> response) {


                if (response.isSuccessful()&&response!=null&&response.body()!=null) {

                    try {

                        Log.v("TAG", response.body().toString());
                        newsModelList=new ArrayList<>();
                        newsModelList.addAll(response.body());
                        recycler.setLayoutManager(new LinearLayoutManagerWrapper(NewsDetailsAllActivity.this));
                        recyclerAdapter=new RecyclerReviews(newsModelList,NewsDetailsAllActivity.this);
                        //recyclerView.setLayoutAnimation(new De);
                        recycler.setAdapter(recyclerAdapter);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<List<ViewReviews>> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }
}
