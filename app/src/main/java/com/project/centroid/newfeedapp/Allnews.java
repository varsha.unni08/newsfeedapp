package com.project.centroid.newfeedapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.project.centroid.newfeedapp.adapter.RecyclerAdapterNews;
import com.project.centroid.newfeedapp.model.District;
import com.project.centroid.newfeedapp.model.NewsItem;
import com.project.centroid.newfeedapp.model.Pincode;
import com.project.centroid.newfeedapp.model.State;
import com.project.centroid.newfeedapp.rest.ApiConstants;
import com.project.centroid.newfeedapp.rest.RestService;
import com.project.centroid.newfeedapp.rest.Utils;
import com.project.centroid.newfeedapp.utils.LocaleHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Allnews extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.reyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    private AlertDialog dialog;
    private Spinner state, dist, pincode;
    private List<NewsItem> newsItems;
    private SharedPreferences sharedpreferences;
    private RecyclerAdapterNews recyclerAdapter;
    boolean allContactsLoaded = false;
    private int pageCount = 1;
   String  sId1, dId1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        sharedpreferences = this.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
        try{
            if(!TextUtils.isEmpty(sharedpreferences.getString(ApiConstants.USERLANG, ""))){
                String lan=sharedpreferences.getString(ApiConstants.USERLANG, "");
                LocaleHelper.setLocale(Allnews.this, lan);
            }}catch (Exception e){
            e.printStackTrace();
        }
        setContentView(R.layout.activity_allnews);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
          /*fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

          sId1="1";
          dId1="1";
        initRecyclerview(null, null);
    }

    @OnClick(R.id.fab)
    public void onViewClicked() {
        showFileChooserDialogCustom();
    }

    private void showFileChooserDialogCustom() {
        final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) this
                .findViewById(R.id.parent)).getChildAt(0);
        View view = getLayoutInflater().inflate(R.layout.layout_filter, viewGroup, false);
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Options");
        state = (Spinner) view.findViewById(R.id.spinnerState);
        dist = (Spinner) view.findViewById(R.id.spinnerDist);
        pincode = (Spinner) view.findViewById(R.id.spinnerPin);
        Button submit = (Button) view.findViewById(R.id.submit);

        getStates();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Utils.sId= state.getSelectedItemPosition();
                    Utils.dId= dist.getSelectedItemPosition();
                    Utils.pId= pincode.getSelectedItemPosition();
                    String sId = ((State) state.getSelectedItem()).getCdState();
                    String dId = ((District) dist.getSelectedItem()).getCdDistrict();
                    sId1=sId;
                    dId1=dId;

                    dialog.cancel();
                    if (sId != null && dId != null) {
                        pageCount=1;
                        initRecyclerview(sId, dId);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        pictureDialog.setView(view);
        dialog = pictureDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(lp);


    }




    private void initRecyclerview(final String sId, String dId) {

        newsItems = new ArrayList<>();
        LinearLayoutManager mLayoutManager = new LinearLayoutManagerWrapper(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);


        RestService.RestApiInterface client = RestService.getClient();

        Call<List<NewsItem>> callBack = client.getAllNews(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), sId, dId, 1);

        callBack.enqueue(new Callback<List<NewsItem>>() {
            @Override
            public void onResponse(Call<List<NewsItem>> call, Response<List<NewsItem>> response) {

                try {
                    newsItems.clear();
                    if(recyclerAdapter!=null){
                   if(recyclerAdapter.getItemCount()>0){
                       recyclerAdapter.notifyDataSetChanged();

                   }
                    }

                    if(response!=null){
                    if (response.body() != null) {

                        List<NewsItem> posts = response.body();

                        if (posts.size() > 0) {

                            newsItems.addAll(posts);
                            // recyclerAdapter.notifyDataSetChanged();
                            recyclerAdapter = new RecyclerAdapterNews(Allnews.this, newsItems, recyclerView);
                            recyclerView.setAdapter(recyclerAdapter);
                            if (!allContactsLoaded) {
                                try {
                                    loadMore(sId1, dId1);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                        } else {

                            newsItems.clear();
                            recyclerAdapter = new RecyclerAdapterNews(Allnews.this, newsItems, recyclerView);
                            recyclerView.setAdapter(recyclerAdapter);
                        }

                    } else {

                        newsItems.clear();
                        recyclerAdapter = new RecyclerAdapterNews(Allnews.this, newsItems, recyclerView);
                        recyclerView.setAdapter(recyclerAdapter);
                    }}else {

                        newsItems.clear();
                        recyclerAdapter = new RecyclerAdapterNews(Allnews.this, newsItems, recyclerView);
                        recyclerView.setAdapter(recyclerAdapter);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<NewsItem>> call, Throwable t) {
                if(recyclerAdapter!=null) {
                    if (recyclerAdapter.getItemCount() > 0) {
                        newsItems.clear();
                        recyclerAdapter = new RecyclerAdapterNews(Allnews.this, newsItems, recyclerView);
                        recyclerView.setAdapter(recyclerAdapter);


                    }
                }}
        });

    }

    //setSearchView();


    private void loadMore(final String sId, final String dId) {
        recyclerAdapter.setOnLoadMoreListener(new RecyclerAdapterNews.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.v("TAG", "loadmore");

                newsItems.add(null);
                recyclerView.post(new Runnable() {
                    public void run() {
                        recyclerAdapter.notifyItemInserted(newsItems.size() - 1);
                    }
                });
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //remove progress item
                     /*   if(newsItems.size()>0){
                        ////newsItems.remove(newsItems.size() - 1);
                        recyclerAdapter.notifyItemInserted(newsItems.size());}
                     */   //add items one by one
/*                        for (int i = 0; i < 15; i++) {
                            myDataset.add("Item" + (myDataset.size() + 1));
                            mAdapter.notifyItemInserted(myDataset.size());

                        }*/
                        Log.v("TAG", dId+"loading"+sId);
                        if (!allContactsLoaded) {

                            pageCount++;

                            try {
                                getMoreData(pageCount, sId1, dId1);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            //recyclerAdapter.setLoaded();

                        }
                        //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
                    }
                }, 2000);
                //System.out.println("load");
            }
        });

    }

    private void getMoreData(final int page,  String sId,  String dId) {
        RestService.RestApiInterface client = RestService.getClient();
        Call<List<NewsItem>> callBack = client.getAllNews(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), sId, dId, page);

        callBack.enqueue(new Callback<List<NewsItem>>() {
            @Override
            public void onResponse(Call<List<NewsItem>> call, Response<List<NewsItem>> response) {

                try {

                    if (!response.body().isEmpty()) {
                        if (response.body() != null) {


                            Log.v("TAG", sId1+"newloading"+dId1);
                            List<NewsItem> posts = response.body();
                            if (posts.size() > 0) {
                                Log.v("TAG", "newloading");
                                recyclerAdapter.setLoaded();
                                newsItems.remove(newsItems.size() - 1);
                                newsItems.addAll(posts);
                                recyclerAdapter.notifyItemInserted(newsItems.size());


                            } else {

                                Log.v("TAG", "loadingfalse");
                                recyclerAdapter.setLoadedFalse();
                                pageCount = 1;
                                allContactsLoaded = true;
                                newsItems.remove(newsItems.size() - 1);
                                recyclerAdapter.notifyDataSetChanged();


                            }
                        } else {
                            Log.v("TAG", "loadingend");

                            allContactsLoaded = true;
                            newsItems.remove(newsItems.size() - 1);
                            recyclerAdapter.setLoadedFalse();
                            recyclerAdapter.notifyDataSetChanged();


                        }
                    }
                        else{
                            allContactsLoaded = true;
                            newsItems.remove(newsItems.size() - 1);
                            recyclerAdapter.setLoadedFalse();
                            recyclerAdapter.notifyDataSetChanged();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<NewsItem>> call, Throwable t) {

            }
        });


    }


    private void getStates() {

        final ProgressDialog progressDialog = new ProgressDialog(this,
                ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("please wait...");
        progressDialog.show();


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<State>> callback = client.getAllStates(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), "state");
        callback.enqueue(new Callback<List<State>>() {
            @Override
            public void onResponse(Call<List<State>> call, Response<List<State>> response) {
                try {
                    progressDialog.cancel();
                    progressDialog.dismiss();
                    String responseBody = response.body().toString();
                    if (responseBody.trim().charAt(0) == '{') {

                    } else if (responseBody.trim().charAt(0) == '[') {


                        ArrayAdapter<State> adapter =
                                new ArrayAdapter<>(Allnews.this, android.R.layout.simple_spinner_dropdown_item, response.body());
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        state.setAdapter(adapter);
                        if(Utils.sId!=null){
                            if(state.getCount()>=Utils.sId){
                            state.setSelection(Utils.sId);}
                        }
                        // ((TextView) state.getSelectedItem).setTextColor(getResources().getColor(R.color.colorPrimaryText));
                        try {

                            state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    getDistrict();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<State>> call, Throwable t) {
                progressDialog.cancel();
                progressDialog.dismiss();
            }
        });
    }

    private void getDistrict() {

        final ProgressDialog progressDialog = new ProgressDialog(this,
                ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        String stat = ((State) state.getSelectedItem()).getCdState();
        if (stat != null) {
            // TODO: Implement your own authentication logic here.
            RestService.RestApiInterface client = RestService.getClient();

            Call<List<District>> callback = client.getAllDistrict(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), "district", stat);
            callback.enqueue(new Callback<List<District>>() {
                @Override
                public void onResponse(Call<List<District>> call, Response<List<District>> response) {

                    try {
                        try {
                            progressDialog.cancel();
                            progressDialog.dismiss();
                            String responseBody = response.body().toString();
                            if (responseBody.trim().charAt(0) == '{') {

                            } else if (responseBody.trim().charAt(0) == '[') {


                                ArrayAdapter<District> adapter =
                                        new ArrayAdapter<>(Allnews.this, android.R.layout.simple_spinner_dropdown_item, response.body());
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                dist.setAdapter(adapter);
                                if(Utils.dId!=null){
                                    if(dist.getCount()>=Utils.dId){
                                dist.setSelection(Utils.dId);}}
                                //((TextView) dist.getSelectedItem()).setTextColor(getResources().getColor(R.color.colorPrimaryText));
                                dist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        getPincode();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<List<District>> call, Throwable t) {
                    progressDialog.cancel();
                    progressDialog.dismiss();
                }
            });
        }
    }

    private void getPincode() {

        final ProgressDialog progressDialog = new ProgressDialog(this,
                ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        String pin = ((District) dist.getSelectedItem()).getCdDistrict();
        if (pin != null) {
            // TODO: Implement your own authentication logic here.
            RestService.RestApiInterface client = RestService.getClient();

            Call<List<Pincode>> callback = client.getAllPincode(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), "pincode", pin);
            callback.enqueue(new Callback<List<Pincode>>() {
                @Override
                public void onResponse(Call<List<Pincode>> call, Response<List<Pincode>> response) {

                    try {
                        progressDialog.cancel();
                        progressDialog.dismiss();
                        if(response!=null){
                        String responseBody = response.body().toString();
                        if (responseBody.trim().charAt(0) == '{') {

                        } else if (responseBody.trim().charAt(0) == '[') {


                            ArrayAdapter<Pincode> adapter =
                                    new ArrayAdapter<>(Allnews.this, android.R.layout.simple_spinner_dropdown_item, response.body());
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            pincode.setAdapter(adapter);
                            if(Utils.pId!=null){
                                if(pincode.getCount()>=Utils.pId){
                            pincode.setSelection(Utils.pId);}
                            }
                            //((TextView) state.getSelectedItem()).setTextColor(getResources().getColor(R.color.colorPrimaryText));

                        }}

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<List<Pincode>> call, Throwable t) {
                    progressDialog.cancel();
                    progressDialog.dismiss();
                }
            });
        }
    }
}
