package com.project.centroid.newfeedapp.adapter;
import android.content.Context;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.project.centroid.newfeedapp.R;
import com.project.centroid.newfeedapp.fragments.FragmentAgriculture;
import com.project.centroid.newfeedapp.fragments.FragmentEntertainment;
import com.project.centroid.newfeedapp.fragments.FragmentEvents;
import com.project.centroid.newfeedapp.fragments.FragmentHealth;
import com.project.centroid.newfeedapp.fragments.FragmentNational;
import com.project.centroid.newfeedapp.fragments.FragmentPolitics;
import com.project.centroid.newfeedapp.fragments.FragmentSport;
import com.project.centroid.newfeedapp.fragments.FragmentSports;
import com.project.centroid.newfeedapp.fragments.FragmentTechnology;

import java.util.ArrayList;
import java.util.List;

public  class NewsPagerAdapter extends FragmentStatePagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final Integer[] mFragmentTitleList = new Integer[]{R.string.tab1,R.string.tab2,R.string.tab8,R.string.tab4,R.string.tab5,R.string.tab6,R.string.tab7,R.string.tab3};
    private Resources resources;

    public  NewsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        resources = context.getResources();
    }

    /** Instantiate fragment based on user horizontal scroll position */
    @Override
    public Fragment getItem(int position) {
        // Url along with API key to query the API
        String category;

        // Fragment for different news section
        Fragment fragment;

        switch (position) {
            case 0:
                // Url for technology news section, re-use the tab title text here

                // Initialize fragment with news section as argument
                fragment = new FragmentSport();

                // Return the fragment
                return fragment;
            case 1:
                // Url for science news section

                // Initialize fragment with news section as argument

                fragment = new FragmentPolitics();
                // Return the fragment
                return fragment;
            case 7:
                // Url for travel news section
                fragment = new FragmentEntertainment();

                // Return the fragment
                return fragment;
            case 3:
                // Url for books news section
                    // Initialize fragment with news section as argument
                fragment = new FragmentTechnology();

                return fragment;
          case 4:
                // Url for books news section
                    // Initialize fragment with news section as argument
                fragment = new FragmentAgriculture();

                return fragment;
                case 5:
                // Url for books news section
                    // Initialize fragment with news section as argument
                fragment = new FragmentHealth();

                return fragment;
        case 6:
                // Url for books news section
                    // Initialize fragment with news section as argument
                fragment = new FragmentEvents();

                return fragment;
           case 2:
                // Url for books news section
                    // Initialize fragment with news section as argument
                fragment = new FragmentNational();

                return fragment;
            default:
                return null;

        }}

        /** Informs the adapter of the total number of available fragments views */
        @Override
        public int getCount () {
            return mFragmentTitleList.length;
        }

  /*  public void addFrag(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }
*/
        /** Set tab title */
        @Override
        public CharSequence getPageTitle ( int position){
            return resources.getString(mFragmentTitleList[position]);
        }
    }