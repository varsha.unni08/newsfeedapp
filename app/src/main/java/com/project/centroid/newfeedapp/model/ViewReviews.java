package com.project.centroid.newfeedapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ViewReviews {

    @SerializedName("cd_news")
    @Expose
    private String cdNews;
    @SerializedName("ds_news_reviews")
    @Expose
    private String dsNewsReviews;
    @SerializedName("ds_star_rating")
    @Expose
    private String dsStarRating;

    /**
     * No args constructor for use in serialization
     *
     */
    public ViewReviews() {
    }

    /**
     *
     * @param dsNewsReviews
     * @param dsStarRating
     * @param cdNews
     */
    public ViewReviews(String cdNews, String dsNewsReviews, String dsStarRating) {
        super();
        this.cdNews = cdNews;
        this.dsNewsReviews = dsNewsReviews;
        this.dsStarRating = dsStarRating;
    }

    public String getCdNews() {
        return cdNews;
    }

    public void setCdNews(String cdNews) {
        this.cdNews = cdNews;
    }

    public String getDsNewsReviews() {
        return dsNewsReviews;
    }

    public void setDsNewsReviews(String dsNewsReviews) {
        this.dsNewsReviews = dsNewsReviews;
    }

    public String getDsStarRating() {
        return dsStarRating;
    }

    public void setDsStarRating(String dsStarRating) {
        this.dsStarRating = dsStarRating;
    }

}